import matplotlib.pyplot as plt
import numpy as np
import itertools
import pynslr
import pandas as pd
import scipy.interpolate

from benchmark_filtering import get_hollywood2

def get_segment_features(segmentation, outliers, dt):
    classfeats = []
    prev_duration = 0.1
    prev_velocity = 1.0
    prev_speed = np.array([[0.0, 0.0]])
    prev_cls = 1
    prev_amplitude = 0
    prev_direction = np.array([[0.0, 0.0]])
    prev_is_valid = False
    for seg in segmentation.segments:
        slc = slice(*seg.i)
        #sd = ed.iloc[slc]
        duration = float(np.diff(seg.t))
        duration += np.random.uniform(-dt/2, dt/2)
        speed = np.diff(seg.x, axis=0)/duration
        direction = speed/np.linalg.norm(speed)
        velocity = np.linalg.norm(speed)
        speed_diff = float(np.linalg.norm(speed - prev_speed))
        direction_diff = float(np.dot(prev_direction, direction.T))
        if direction_diff != direction_diff:
            direction_diff = 0
        
        # Fisher transform
        direction_diff *= (1 - 1e-6) # Avoid -1 and 1
        direction_diff = np.arctanh(direction_diff)
        is_valid = not np.any(outliers[slc])
        #print(direction_diff)
        amplitude = velocity*duration
        #if cls == 2 and (prev_cls == 2 or prev_cls == 22):
        #    cls = 22
        #print(velocity)
        classfeats.append(dict(
            duration=duration,
            velocity=velocity,
            speed_diff=speed_diff,
            direction_diff=direction_diff,
            prev_velocity=prev_velocity,
            prev_duration=prev_duration,
            amplitude=amplitude,
            prev_amplitude=prev_amplitude,
            prev_cls=prev_cls,
            ts=seg.t[0],
            is_valid=is_valid,
            prev_is_valid=prev_is_valid
            ))
       
        prev_duration = duration
        prev_velocity = velocity
        prev_speed = speed
        prev_amplitude = amplitude
        prev_direction = direction
        prev_is_valid = is_valid
    return classfeats

classfeats = []
for ts, xs, outliers in itertools.islice(get_hollywood2(min_dur=3.0), 100):
    dt = np.median(np.diff(ts))
    xs = scipy.interpolate.interp1d(ts[~outliers], xs[~outliers], axis=0, fill_value='extrapolate')(ts)
    segs = pynslr.fit_gaze(ts, xs, 0.1)
    feats = get_segment_features(segs, outliers, dt)
    classfeats.extend(feats)
    #print(feats)
    continue
    st = np.array(segs.t)
    sx = np.array(segs.x)
    ax = plt.subplot(2,1,1)
    plt.plot(ts[~outliers], xs[~outliers][:,0])
    plt.plot(st, sx[:,0], 'o-')
    #plt.plot(ts, sig[:,0], 'k.', alpha=0.1)
    plt.subplot(2,1,2, sharex=ax)
    plt.plot(ts[~outliers], xs[~outliers][:,1])
    plt.plot(st, sx[:,1], 'o-')
    #plt.plot(ts, sig[:,1], 'k.', alpha=0.1)
    plt.show()

from matplotlib.patches import Ellipse
def plot_ellipse(means, cov, **kwargs):
    vals, vecs = np.linalg.eig(cov)
    order = np.argsort(vals)[::-1]
    nstd = 2
    vals = vals[order]
    vecs = vecs[:,order]
    theta = np.degrees(np.arctan2(*vecs[:,0][::-1]))
    w, h = 2 * nstd * np.sqrt(vals)
    ell = Ellipse(xy=means, width=w, height=h,
            angle=theta, **kwargs)
    ell.set_facecolor('none')
    plt.gca().add_artist(ell)
    #print(evecs)

classfeats = pd.DataFrame.from_records(classfeats)
classfeats = classfeats[classfeats.is_valid]
classfeats = classfeats[classfeats.prev_is_valid]
classfeats = classfeats[classfeats.velocity > 1e-6] 
#plt.hist(classfeats['speed_diff'], bins=500)
import matplotlib
plt.gca().xaxis.set_major_formatter(
        matplotlib.ticker.FuncFormatter(lambda x, pos: 10**x)
        )
plt.gca().yaxis.set_major_formatter(
        matplotlib.ticker.FuncFormatter(lambda x, pos: 10**x)
        )

from sklearn.decomposition import PCA
from sklearn.preprocessing import robust_scale
feats = np.array([
    np.log10(classfeats['velocity']),
    classfeats['direction_diff']
    ]).T

plt.plot(feats[:,0], feats[:,1], '.', alpha=0.1)
from sklearn.mixture import GaussianMixture
n_components = 6
model = GaussianMixture(n_components, covariance_type='diag').fit(feats)

from pprint import pprint
params = list(zip(model.means_, model.covariances_))
order = np.argsort([p[0][0] for p in params])
labels = [1, 4, 3, 2, None, None]
for i, comp in enumerate(order):
    m, v = params[comp]
    print(labels[i], ':', [(m).tolist(), (v).tolist()], ',')
    m = m[:2]; v = v[:2]
    plot_ellipse(m, np.diag(v), color='black')
    plt.plot(m[0], m[1], 'o')
#plt.hexbin(np.log10(classfeats['velocity']), classfeats.direction_diff, gridsize=100)
plt.show()
#print(len(classfeats))
