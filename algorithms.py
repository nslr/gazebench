import numpy as np
from scipy.interpolate import interp1d

def dispersion_threshold(threshold=3.0, window_size=5):
    def algo(ts, x):
        is_fixation = np.zeros(len(x), dtype=np.bool)
        fixations = []
        s = 0
        e = window_size
        while True:
            if e >= len(x):
                break
            dispersion = np.sum(np.ptp(x[s:e], axis=0))
            if dispersion > threshold:
                s += 1
                e = s + window_size
                continue
            e += 1
            while e < len(x):
                dispersion = np.sum(np.ptp(x[s:e], axis=0))
                if dispersion > threshold:
                    fixations.append(s)
                    s = e
                    e = s + window_size
                    continue
                e += 1
        
        return ts[fixations]

    return algo

def velocity_threshold(threshold=100.0):
    def algo(ts, x):
        dt = np.diff(ts)
        dx = np.diff(x, axis=0)
        dist = np.sqrt(np.sum(dx**2, axis=1))
        speeds = dist/dt
        #return ts[get_fixations(speeds < threshold)]

        fixation_idx = np.flatnonzero(speeds < threshold)
        fixation_edges = np.diff(fixation_idx) > 1
        
        fixation_idx = (fixation_idx[np.flatnonzero(fixation_edges)]) + 1
        # Hack!
        fixation_idx = sorted(list(fixation_idx) + list(fixation_idx + 1))
        if len(fixation_idx) == 0:
            return []
        return ts[np.unique(fixation_idx)]
    return algo

def get_fixations(is_fixation):
        fixation_idx = np.flatnonzero(is_fixation)
        fixation_edges = np.diff(fixation_idx) > 1
        return fixation_idx[fixation_edges] + 1
    

# Optimal continuous linear segmentation
# for one dimensional signal
def fit_segments1d(ts, xs, sts, sis):
    """
    sum = np.sum
    ts = np.array(ts)
    xs = np.array(xs)
    sts = list(sts)
    if len(sts) == 0 or sts[0] != ts[0]:
        sts.insert(0, ts[0])
    sts.append(ts[-1])

    n = len(ts)
    """
    m = len(sts) - 1

    sis = ts.searchsorted(sts)
    sis[-1] += 1
    b = np.zeros(m+1)
    A = np.zeros((m+1, m+1))
    
    for i in range(m):
        si = sis[i]
        ei = sis[i+1]
        s = slice(si, ei)

        T0 = sts[i]
        T1 = sts[i+1]
        x = xs[s]
        r = (ts[s] - T0)/(T1 - T0)

        A[i,i] += sum((1-r)**2)
        b[i] += sum(x*(1-r))
        A[i][i+1] += sum(r*(1-r))
        A[i+1][i+1] += sum(r**2)
        b[i+1] += sum(x*r)

        A[i+1][i] += sum((1-r)*r)
    
    b = np.array(b).reshape(-1, 1)
    result = np.dot(np.linalg.inv(A), b).reshape(-1)
    return result

def fit_fixations(ts, xs, sts):
    ts = np.array(ts)
    xs = np.array(xs)
    sts = list(sts)

    start = []
    if len(sts) == 0 or sts[0] != ts[0]:
        sts = sts
        start = [ts[0]]
        sts.insert(0, ts[0])
    sts.append(ts[-1])

    n = len(ts)
    m = len(sts) - 1
    
    sts = np.array(sts)
    sis = ts.searchsorted(sts)
    sis[-1] += 1
    results = []
    times = []
    for i in range(m):
        times.extend([ ts[sis[i]], ts[sis[i+1] - 1] ])
        mean = np.mean(xs[sis[i]:sis[i+1]], axis=0)
        results.extend([mean]*2)

    return interp1d(times, results, axis=0, kind='nearest')

def fit_independent_segments(ts, xs, sis):
    #sis = ts.searchsorted(sts)
    endtimes = []
    endpoints = []
    
    #sis = list(ts.searchsorted(sts))
    sis = [0] + list(sis) + [len(ts)]
    sis = np.unique(sis)
    
    for i in range(len(sis) - 1):
        start = sis[i]
        end = sis[i+1]

        if end - start < 2:
            end += 1
        span = slice(start, end)
        t = ts[span]
        x = xs[span]
        fit_0 = np.poly1d(np.polyfit(t, x[:,0], 1))
        fit_1 = np.poly1d(np.polyfit(t, x[:,1], 1))
        fit = lambda t: [fit_0(t), fit_1(t)]
        endtimes.append(t[0])
        endpoints.append(fit(t[0]))
        endtimes.append(t[-1])
        endpoints.append(fit(t[-1]))

    fit = interp1d(endtimes, endpoints, axis=0)
    
    #import matplotlib.pyplot as plt
    #plt.plot(ts, fit(ts)[:,0])
    #plt.plot(ts, xs[:,0], '.')
    #plt.show()
    
    return fit

def fit_segments(ts, xs, sts):
    ts = np.array(ts)
    xs = np.array(xs)
    sts = list(sts)
    if len(sts) == 0 or sts[0] != ts[0]:
        sts.insert(0, ts[0])
    sts.append(ts[-1])

    n = len(ts)
    m = len(sts) - 1
    
    sts = np.array(sts)
    sis = ts.searchsorted(sts)
    sis[-1] += 1

    endpoints = []
    if len(xs.shape) == 1:
        xs = xs.reshape(-1, 1)
    for dim in range(xs.shape[1]):
        dp = fit_segments1d(ts, xs[:,dim], sts, sis)
        
        endpoints.append(dp)

    endpoints = np.array(endpoints).T
    return interp1d(sts, endpoints, axis=0)

    sum = np.sum
    ts = np.array(ts)
    xs = np.array(xs)
    sts = list(sts)
    if len(sts) == 0 or sts[0] != ts[0]:
        sts.insert(0, ts[0])
    sts.append(ts[-1])

    n = len(ts)
    m = len(sts) - 1

    sis = ts.searchsorted(sts)
    sis[-1] += 1
    b = np.zeros(m+1)
    A = np.zeros((m+1, m+1))
    
    for i in range(m):
        si = sis[i]
        ei = sis[i+1]
        s = slice(si, ei)

        T0 = sts[i]
        T1 = sts[i+1]
        x = xs[s]
        r = (ts[s] - T0)/(T1 - T0)

        A[i,i] += sum((1-r)**2)
        b[i] += sum(x*(1-r))
        A[i][i+1] += sum(r*(1-r))
        A[i+1][i+1] += sum(r**2)
        b[i+1] += sum(x*r)

        A[i+1][i] += sum((1-r)*r)
    
    b = np.array(b).reshape(-1, 1)
    result = np.dot(np.linalg.inv(A), b)
    return interp1d(sts, result.flatten())

