import itertools
import fast_saccade_detectors as falgo
import algorithms as algo
import simulate
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import time
import datetime
import cppimport
pynslr = cppimport.imp("pynslr")
import scipy.interpolate
import sys
import multiprocessing

def pmap(func, itr):
    threads = []
    for arg in itr:
        thread = threading.Thread(target=lambda: func(arg))
        thread.start()
        print("Starting", arg)
        threads.append(thread)

    for thread in threads:
        thread.join()


#noise_levels = np.linspace(0.01, 10, 10)
#noise_levels = [0.1, 0.5, 1.0, 3.0][::-1]
noise_levels = np.geomspace(0.05, 5.0, 5)
#noise_levels = [1.0]
#noise_levels = [1.0]
#fixation_durations = np.linspace(0.1, 1.0, 10)
#fixation_durations = [0.3]
#nystagmus_areas = [5, 10, 20]
#nystagmus_areas = np.geomspace(5, 20, 3)
max_distances = np.linspace(2, 60, 3)
max_speeds = [1, 10, 40]
#max_durations = [0.3, 1.0, 3.0]
max_durations = np.linspace(0.3, 5.0, 3)
#nystagmus_areas = [5.0]
#sampling_rates = [30, 60, 120, 500, 1000][::-1]
#sampling_rates = [30, 120, 500][::-1]
sampling_rates = [120]
#sampling_rates = np.geomspace(30, 500, 5)[::-1]
print(sampling_rates)
#sampling_rates = np.linspace(30, 1000, 5)
duration = 60*2
trials = list(range(1))
#penalties = np.linspace(0.0, 20, 30)
penalties = np.linspace(0.0, 20, 10)

result = []
#params = noise_levels, nystagmus_areas, sampling_rates, trials
params = noise_levels, max_distances, max_speeds, max_durations, sampling_rates, trials
total_rows = np.product([len(p) for p in params])
target_cache = {}
params = itertools.product(*params)
start_time = time.time()
for i, (noise_level, max_distance, max_speed, max_duration, sampling_rate, trial) in enumerate(params):
    sparams = (max_distance, max_speed, max_duration, trial)
    if sparams in target_cache:
        target, eye, eye_interp = target_cache[sparams]
    else:
        target = simulate.random_nystagmus(max_distance=[max_distance]*2, max_duration=max_duration, max_speed=max_speed)
        n_samples = int(duration/simulate.DT)
        target = np.array(simulate.take(target, n_samples))
        eye = target.copy()
        eye = np.array(list(simulate.eye_filter(target)))
        eye_interp = scipy.interpolate.interp1d(eye[:,0], eye, axis=0)
        target_cache[sparams] = target, eye, eye_interp

    #target = simulate.random_targets()
    
    #eye = target.copy()
    #eye = np.array(list(simulate.eye_filter(target)))
    #eye_interp = scipy.interpolate.interp1d(eye[:,0], eye, axis=0)
    new_ts = np.arange(0, eye[:,0][-1], 1.0/sampling_rate)
    eye = eye_interp(new_ts)
    #sampling_ratio = int((1.0/simulate.DT)/sampling_rate)
    #eye = eye[::sampling_ratio]
    signal = eye.copy()
    signal[:,1] += np.random.randn(len(signal))*noise_level
    signal[:,2] += np.random.randn(len(signal))*noise_level
    isnrs = []

    
    def est_penalty(penalty):
        
        #split = falgo.nols_custom(signal[:,0], signal[:,1:], noise_std=[noise_level, noise_level],
        #       split_multiplier=penalty)
        #split = signal[:,0][split]
        #fit = algo.fit_fixations(signal[:,0], signal[:,1:], split)
        #fit = algo.fit_independent_segments(signal[:,0], signal[:,1:], split)
    
        #ft, fx = pynslr.nslr2d(
        #        signal[:,0], signal[:,1:],
        #        pynslr.Nslr2d([noise_level, noise_level], pynslr.penalized_exponential_split(penalty)),
        #        )
        pf = pynslr.constant_penalty_split(penalty)
        model = pynslr.Nslr2d([noise_level]*2, pf)
        fit = pynslr.nslr2d(signal[:,0], signal[:,1:], model)
        #fit = reconstruct(signal[:,0], segments)
        #fit = scipy.interpolate.interp1d(ft, fx, axis=0)

        recon = fit(eye[:,0])
        fiterr = lambda est: np.mean(np.linalg.norm(est - eye[:,1:], axis=1)**2)
        error = fiterr(fit(eye[:,0]))
        noise = fiterr(signal[:,1:])
        if error != error:
            raise ValueError("Nans in reconstruction!")
        #print(error, noise)
        #error = np.mean(np.sum(np.linalg.norm(fit(eye[:,0]) - eye[:,1:], axis=0))
        #error = np.mean((np.sum((fit(eye[:,0]) - eye[:,1:])**2, axis=1)))
        #noise = np.mean((np.sum((signal[:,1:] - eye[:,1:])**2, axis=1)))
        isnr = 10*np.log10(noise/error)
        #isnrs.append((penalty, noise_level, nystagmus_area, sampling_rate,trial,noise,error,isnr))
        #print(",".join(map(repr, isnrs[-1])))
        
        #sys.stdout.flush()
        
        #return (penalty, noise_level, max_distance, max_speed, max_duration, sampling_rate,trial,noise,error,isnr)
        print(penalty, isnr)
        #split_t = signal[:,0][split]
        plt.plot(signal[:,0], fit(signal[:,0])[:,0], 'r-')
        #plt.plot(split_t, fit(split_t)[:,0], 'o')
        plt.plot(signal[:,0], signal[:,1], 'k.', alpha=0.3)
        plt.plot(eye[:,0], eye[:,1], 'g-')
        plt.plot(target[:,0], target[:,1])
        plt.show()
        return (penalty, noise_level, max_distance, max_speed, max_duration, sampling_rate,trial,noise,error,isnr)
    

    #list(pool.imap_unordered(est_penalty, penalties))
    #pmap(est_penalty, penalties)
    pres = []
    for j, penalty in enumerate(penalties):
        res = est_penalty(penalty)
        pres.append(res)
        result.append(res)
        row = i*len(penalties) + j + 1
        share_done = row/(total_rows*len(penalties))
        print("%i/%i = %.2f%%"%(row, total_rows*len(penalties), share_done*100))
        print(result[-1])
        dur = time.time() - start_time
        if share_done > 0:
            est_total_dur = dur/share_done
            print("Est time left:", datetime.timedelta(seconds=est_total_dur - dur))

    #result.extend(isnrs)
    #isnrs = np.rec.fromrecords(isnrs, names='penalty,noise_level,nystagmus_area,sampling_rate,trial,noise,error,isnr')
    continue
    isnrs = np.rec.fromrecords(pres, names='penalty,noise_level,max_distance,max_speed,max_duration,sampling_rate,trial,noise,error,isnr')
    plt.figure(0)
    plt.plot(isnrs.penalty, isnrs.isnr)
    best = np.argmax(isnrs.isnr)
    plt.plot(isnrs.penalty[best], (isnrs[best].isnr), 'o')
    plt.figure(1)
    plt.plot(1.0/isnrs[best].sampling_rate, (isnrs[best].penalty), 'o')
    plt.show()
    #target = target[
    #plt.plot(eye[:,0], eye[:,1])
    #plt.plot(signal[:,0], signal[:,1], '.')
result = np.rec.fromrecords(result, names='penalty,noise_level,max_distance,max_speed,max_duration,sampling_rate,trial,noise,error,isnr')
pd.DataFrame(result).to_csv('nystagmus_results.csv')
#result.tofile('nystagmus_results.npy')
#plt.show()
