import os
import pandas as pd
import glob
import scipy.interpolate
data = []

def dirs(path):
    dirs = sorted([os.path.join(path, d) for d in os.listdir(path)])
    dirs = filter(os.path.isdir, dirs)
    return dirs

for subject_dir in dirs('datasets/etra2016-ibdt-dataset'):
    subject = os.path.basename(subject_dir)
    for session_dir in dirs(subject_dir):
        session = os.path.basename(session_dir)
        journal = glob.glob(session_dir + '/journal-*.txt')[0]
        journal = pd.read_csv(journal, sep='\t', skiprows=1)
        labeling = pd.read_csv(os.path.join(session_dir, 'reviewed.csv'), names='index,gaze_type,timestamp'.split(','))
        gaze_type_interp = scipy.interpolate.interp1d(labeling.timestamp.values, labeling.gaze_type.values, kind='nearest')
        journal['gaze_type'] = gaze_type_interp(journal.timestamp)
        #data = journal.join(labeling, on='index', rsuffix='_l')
        import matplotlib.pyplot as plt
        
        #data = data[data.eye_valid == 1]
        #plt.plot(data.timestamp, data.eye_x)
        journal = journal[journal.eye_valid == 1]
        plt.plot(journal.timestamp, journal.eye_x)
        plt.twinx()
        plt.plot(journal.timestamp, journal.gaze_type)
        #plt.plot(data.timestamp, data.gaze_type)
        #plt.plot(data.timestamp, data.field_x)
        plt.show()
        print data
        
