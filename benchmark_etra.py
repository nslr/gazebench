import os
import pandas as pd
import glob
import scipy.interpolate
import matplotlib.pyplot as plt
import numpy as np
import cppimport
import pickle
pynslr = cppimport.imp('pynslr')

def unjitter(vals):
    frameLatency = len(vals)
    vals = vals.astype(float)
    diffs = np.abs(np.diff(vals))
    minVal = np.min(diffs[diffs > 0])

    for i in range(1, len(vals)):
        valDiff = np.abs(np.diff([i-1,i]))
        if valDiff == 0 or valDiff > minVal:
            continue

        for j in range(i + 1, min(len(vals), i+frameLatency)):
            if vals[j] == vals[i]: continue
            if vals[j] == vals[i-1]:
                vals[i] = vals[i-1]
            break

    return np.array(vals)

def dispersion(x, y):
    return np.sqrt(np.ptp(x)**2 + np.ptp(y)**2)

def removeOneSampleSpike(x, y):
    dx = np.ediff1d(x, to_begin=0)
    dy = np.ediff1d(y, to_begin=0)
    ds = np.sqrt(dx**2 + dy**2)
    minVal = np.min(ds[ds > 0])

    for i in range(1, len(ds) - 1):
        if ds[i] <= minVal: continue
        nx = [x[i-1], x[i+1]]
        ny = [y[i-1], y[i+1]]
        if dispersion(nx, ny) <= minVal:
            x[i] = np.round(np.median(nx))
            y[i] = np.round(np.median(ny))
    
    return x, y

def paddiff(s, **kwargs):
    ds = []
    for v in s.T:
        ds.append(np.ediff1d(v, **kwargs))
    return np.vstack(ds).T

def load_etra():
    def dirs(path):
        dirs = sorted([os.path.join(path, d) for d in os.listdir(path)])
        dirs = filter(os.path.isdir, dirs)
        return dirs

    for subject_dir in dirs('datasets/etra2016-ibdt-dataset'):
        subject = os.path.basename(subject_dir)
        for session_dir in dirs(subject_dir):
            session = os.path.basename(session_dir)
            journal = glob.glob(session_dir + '/journal-*.txt')[0]
            journal = pd.read_csv(journal, sep='\t', skiprows=1)
            #journal.eye_y /= 2.0 # As per Dikabilis docs, this gives pixels
            journal.eye_y = journal.eye_y.astype(np.float)
            labeling = pd.read_csv(os.path.join(session_dir, 'reviewed.csv'), names='index,gaze_type,timestamp'.split(','))
            ibdt_classes = pd.read_csv(os.path.join(session_dir, 'classification.csv'), names='index,gaze_type,timestamp'.split(','))
            journal['ibdt_classes'] = ibdt_classes['gaze_type']
            #gaze_type_interp = scipy.interpolate.interp1d(labeling.timestamp.values, labeling.gaze_type.values, kind='nearest')
            #journal.eye_x = unjitter(journal.eye_x.values)
            #journal.eye_y = unjitter(journal.eye_y.values)
            #plt.plot(journal.eye_x)
            #journal.eye_x, journal.eye_y = removeOneSampleSpike(journal.eye_x.values, journal.eye_y.values)
            #plt.plot(journal.eye_x)
            #plt.show()
            #plt.plot(journal.timestamp, journal.eye_x, '.')
            #for t in labeling.timestamp:
            #    plt.axvline(t)
            #plt.show()
            journal['gaze_type'] = labeling.gaze_type # gaze_type_interp(journal.timestamp)
            #data = journal.join(labeling, on='index', rsuffix='_l')
        
            #data = data[data.eye_valid == 1]
            #plt.plot(data.timestamp, data.eye_x)
            journal = journal[journal.eye_valid == 1]
            journal = journal[journal.gaze_type != 3]
            yield (subject, session), journal
            continue
            plt.plot(journal.timestamp, journal.eye_x)
            plt.twinx()
            plt.plot(journal.timestamp, journal.gaze_type)
            #plt.plot(data.timestamp, data.gaze_type)
            #plt.plot(data.timestamp, data.field_x)
            plt.show()

def plot_classed(ts, gaze, cls, marker='.', name=''):
    data = pd.DataFrame(dict(timestamp=ts, eye_x=gaze[:,0], eye_y=gaze[:,1], gaze_type=cls))
    for c, d in data.groupby(data.gaze_type):
        if c == 3: continue
        ax = plt.subplot(2,1,1)
        plt.plot(d.timestamp, d.eye_x, marker, color=CLS_COLOR[c], label="%s/%s"%(name, c))
        plt.subplot(2,1,2, sharex=ax)
        plt.plot(d.timestamp, d.eye_y, marker, color=CLS_COLOR[c], label="%s/%s"%(name, c))

FIXATION = 0
SACCADE = 1
PURSUIT = 2
CLASSES = [FIXATION, SACCADE, PURSUIT]
CLS_COLOR = {
    FIXATION: 'blue',
    SACCADE: 'red',
    PURSUIT: 'green'
}
def cperf(est, truth, c):
        tp = np.sum((est == c) & (truth == c))
        fp = np.sum((est == c) & (truth != c))
        tn = np.sum((est != c) & (truth != c))
        fn = np.sum((est != c) & (truth == c))
        recall = tp/(tp + fn)
        precision = tp/(tp + fp)
        specificity = tn/(tn + fp)
        accuracy = (tp + tn)/(tp + tn + fp + fn)
        return [recall, precision, specificity, accuracy]
def performance(cls):
        r = []
        for c in CLASSES:
            #tp = np.sum((cls.machine == c) & (cls.human == c))
            #fp = np.sum((cls.machine == c) & (cls.human != c))
            #tn = np.sum((cls.machine != c) & (cls.human != c))
            #fn = np.sum((cls.machine != c) & (cls.human == c))
            #recall = tp/(tp + fn)
            #precision = tp/(tp + fp)
            #specificity = tn/(tn + fp)
            #accuracy = (tp + tn)/(tp + tn + fp + fn)
            r.append(['nslr', c] + cperf(cls.machine, cls.human, c))
            r.append(['ibdt', c] + cperf(cls.ibdt_classes, cls.human, c))
            #r.append((c, recall, precision, specificity, accuracy))
        return pd.DataFrame.from_records(r, columns="algorithm,class,recall,precision,specificity,accuracy".split(','))

def class_interp(ts, cls):
    ts = np.array(ts)
    cls = np.array(cls)
    def interp(nts):
        return cls[np.clip(ts.searchsorted(nts), 0, len(ts) - 1)]
    return interp

class DummyClass:
    def predict(self, feats):
        dur = np.asarray(feats, dtype=float)[:,0]
        speed = np.asarray(feats, dtype=float)[:,1]
        speed = np.log1p(speed)
        cls = np.zeros(speed.shape, dtype=np.int)
        cls[speed > 2.7] = PURSUIT
        cls[speed > 5.2] = SACCADE
        #cls[dur < 0.1] = SACCADE
        #cls[speed > 5.0] = SACCADE
        return cls

def benchmark():
    allspeeds = []
    allclasses = []
    estclasses = []
    eventfeats = []
    classfeats = []
    results = []
    classer = pickle.load(open('etra_classifier.pickle', 'rb'))
    #classer = DummyClass()
    for (subject, setting), session in load_etra():
        gaze = np.vstack((session.eye_x, session.eye_y)).T
        gaze += np.random.uniform(-1.5, 1.5, gaze.shape)
        t0 = session.timestamp.values[0]
        ts = (session.timestamp.values - t0)/1000.0
        #print(len(gaze))
        #print(len(gaze))
        noise = np.array([5.0, 5.0])
        seen_noises = [noise]
        penalty = 10.0
        while True:
            fit = pynslr.nslr2d(ts, gaze, noise, penalty)
            #fit = scipy.interpolate.interp1d(sts, sxs, axis=0)
            diff = fit(ts) - gaze
            diff = diff[np.isfinite(diff.sum(axis=1))]
            diff = np.abs(diff) + 0.5 # Discretization error
            noise = np.sqrt(np.mean((diff)**2, axis=0))
            diffs = np.linalg.norm(seen_noises - noise, axis=1)
            if np.min(diffs) < 1e-6:
                break
            seen_noises.append(noise)
            #print(noise)
        #noise = 1.5
        #print(noise)
        fit = pynslr.nslr2d(ts, gaze, noise, penalty)
        #fitter = scipy.interpolate.interp1d(fit.t, np.array(fit.x), axis=0)
        fitter = fit
        classes = []
        #for i in range(0, len(sts) - 2, 2):
        prev_cls = SACCADE
        #while i < len(sts) - 1:
        
        segments = list(fit.segments)
        """
        isegments = []
        import copy
        for i in range(len(segments) - 1):
            s, ns = segments[i], segments[i+1]
            isegments.append(s)
            fs = copy.deepcopy(s)
            fs.i = s.i[1], ns.i[0]
            fs.t = s.t[1], ns.t[0]
            fs.x = s.x[1], ns.x[0]
            isegments.append(fs)
            #isegments.append(segments[i])
            #isegments.append
        isegments.append(segments[-1])
        segments = isegments
        """
        
        prev_dur = 0.5
        prev_vel = 0.0
        for i in range(0, len(segments)):
            s = segments[i]
            
            amp = float(np.linalg.norm(np.diff(s.x, axis=0)))
            dur = float(np.diff(s.t))
            vel = float(amp/dur)
            cls = classer.predict([[dur, vel, prev_dur, prev_vel]])[0]
            
            classes.append([s.t[0], cls])
            classes.append([s.t[1], cls])
            
            true_cls = session.gaze_type.values[s.i[0]:s.i[1]]
            w = 1.0/len(true_cls)
            for c in true_cls[1:]:
                classfeats.append((dur, amp, vel, c, w, prev_dur, prev_vel))
            eventfeats.append([dur, amp, vel, cls, prev_dur, prev_vel])
            prev_dur = dur
            prev_vel = vel
            continue

            span = slice(*s.i)
            t = np.array(s.t)
            #x = np.array(s.x).T
            x = fitter(t).T
            amp = np.sum(np.linalg.norm(np.diff(fitter(ts[span]), axis=0), axis=1))

            #amp = np.linalg.norm(x[:,1] - x[:,0])
            if amp != amp:
                raise "Hell!"
            dur = np.diff(t)
            vel = float(amp/dur)
            cls = classer.predict([[dur, vel]])[0]

            #print(np.diff(t), vel)
            print(s.i)
            true_cls = session.gaze_type.values[s.i[0]:s.i[1]]
            w = 1.0/len(true_cls)
            for c in true_cls[1:]:
                classfeats.append((float(np.diff(t)), amp, vel, c, w))
            eventfeats.append([float(np.diff(t)), amp, vel, cls])
            
            #lagged_saccade_sample = False
            #classes.append([t[0], SACCADE])
            #classes.append([t[0] + 1e-6, cls])
            #classes.append([t[1] - 1e-6, cls])
            lagged_saccade_sample = False
            if i > 0:
                ps = fit.segments[i - 1]
                pt = ts[fit.segments[i - 1].i[1] - 2]
                px = fitter(pt).T
                dur = s.t[0] - pt
                amp = np.linalg.norm(x[:,0] - px)
                vel = amp/dur
                if(classer.predict([[dur, vel]])[0] == SACCADE):
                    lagged_saccade_sample = True
                            
            if lagged_saccade_sample:
                classes.append([t[0], SACCADE])
            
            classes.append([t[0], cls])
            classes.append([t[1], cls])

            prev_cls = cls
            
            
            #ax = plt.subplot(2,1,1)
            #plt.plot(t, x[:,0], color='black')
            #plt.subplot(2,1,2, sharex=ax)
            #plt.plot(t, x[:,1], color='black')
        
        
        
        #fit = scipy.interpolate.interp1d(sts, sxs, axis=0)
        #speeds = np.linalg.norm(np.diff(sxs, axis=0), axis=1)/np.diff(sts)
        #print(np.sum(np.gradient(ts) == 0))
        fitspeed = np.linalg.norm(paddiff(fit(ts), to_begin=1.0), axis=1)/np.ediff1d(ts, to_begin=1.0)
        fitaccel = np.abs(np.ediff1d(fitspeed, to_end=0.0)/np.ediff1d(ts, to_end=1.0))
        #fitspeed = np.linalg.norm(np.ediff1d(fit(ts), axis=0), axis=1)/np.gradient(ts)
        
        #print(np.sum(~np.isfinite(fitspeed)))
        allclasses.extend(session.gaze_type)
        """
        ct = ts
        cls = np.zeros(len(ts), dtype=np.int)
        cls[fitspeed > np.expm1(3.0)] = PURSUIT
        cls[fitspeed > np.expm1(4.0)] = SACCADE
        classes = list(zip(ct, cls))
        """
        classes = np.array(classes)
        catchups = []
        in_pursuit = False
        for i in range(len(classes)):
            if classes[i][1] == PURSUIT:
                in_pursuit = True
            if classes[i][1] == FIXATION:
                in_pursuit = False
            if in_pursuit and classes[i][1] == SACCADE:
                catchups.append(i)
        #classes = np.delete(classes, catchups, axis=0)
        ct, cls = zip(*classes)

        #ecls = scipy.interpolate.interp1d(ct, cls, kind='nearest', fill_value='extrapolate')(ts)
        ecls = class_interp(ct, cls)(ts)
        estclasses.extend(ecls)
        res = pd.DataFrame(dict(human=session.gaze_type, machine=ecls, ibdt_classes=session.ibdt_classes))
        res['subject'] = subject
        res['setting'] = setting
        results.append(res)
        #plt.plot(ecls, color='red')
        #plt.plot(session.gaze_type, color='green')
        #plt.show()
        allspeeds.extend(fitspeed)
        #plt.plot(fitspeed)
        #plt.show()
        #diff = fit(ts) - gaze
        continue
        perf = performance(res)
        saccade_g = perf[perf['class'] == 1][perf.algorithm == 'nslr']['recall']
        print(saccade_g)
        #sts, sxs = map(np.array, zip(*((s.t, s.x) for s in fit.segments)))
        sts = ts
        sxs = fitter(ts)
        ax = plt.subplot(2,1,1)
        plt.plot(fitter.t, np.array(fitter.x)[:,0], 'ko-')
        plt.subplot(2,1,2, sharex=ax)
        plt.plot(fitter.t, np.array(fitter.x)[:,1].T, 'ko-')
        plot_classed(ts, gaze, ecls, 'o', name='nslr')
        plot_classed(ts, gaze, session.gaze_type, name='human')
        plt.legend()
        plt.show()
        #plt.plot(session.timestamp, session.eye_x)
        #plt.plot(sts, sxs[:,0])


    d = pd.DataFrame(dict(speed=allspeeds, gaze_type=allclasses))
    cls = pd.concat(results)
    #cls = pd.DataFrame(dict(human=allclasses, machine=estclasses))
    #allspeeds = np.array(allspeeds)
    #allspeeds = allspeeds[np.isfinite(allspeeds)]
    


    #print(c, recall, precision, specificity, accuracy)
    #print(cls.groupby(['subject', 'setting']))
    
    perf = cls.groupby(['subject', 'setting']).apply(performance).reset_index()
    del perf['level_2']
    perf = perf.dropna()#.groupby('algorithm')
    #print(perf.dropna())
    print(perf.groupby('algorithm').mean()*100)
    print(perf.groupby('algorithm').std()*100)
    print(perf.groupby(['algorithm', 'class']).mean()*100)
    print(perf.groupby(['algorithm', 'class']).std()*100)
    
    valid = np.isfinite(d.speed.values)
    logspeed = np.log(d.speed.values + 1.0)[valid]
    
    eventfeats = pd.DataFrame.from_records(eventfeats, columns="dur,amp,vel,cls,prev_dur,prev_vel".split(','))
    #speed_vs_dur = eventfeats[:,0]/eventfeats[:,1]
    
    #amplitude = eventfeats[:,0]*eventfeats[:,1]
    #plt.plot(amplitude, eventfeats[:,1], '.')
    #plt.hist(np.log((eventfeats[:,1] + 1.0)/(amplitude + 1.0)), bins=100)
    #plt.show()
    from sklearn.mixture import GaussianMixture
    from sklearn.naive_bayes import GaussianNB
    from sklearn.svm import SVC
    from sklearn.pipeline import make_pipeline
    from sklearn.preprocessing import FunctionTransformer, StandardScaler
    f = eventfeats[['dur', 'vel', 'prev_dur', 'prev_vel']].copy()

    classfeats = pd.DataFrame.from_records(classfeats, columns="dur,amp,vel,cls,weight,prev_dur,prev_vel".split(','))
    
    for c, cf in classfeats.groupby('cls'):
        plt.plot(np.log1p(cf.dur), np.log1p(cf.vel), '.', label=cls, alpha=0.1)
    plt.show()

    #mindur = np.min(eventfeats['dur'])
    #print(np.random.uniform(len(f))*np.min(f.dur)/2.0)
    #f.dur += np.random.uniform(0, dt/2.0, size=len(f))
    #f.amp = np.log(f.amp + 1.0)
    #model = GaussianMixture(n_components=3)
    model = SVC()
    #model = GaussianNB()
    scaler = StandardScaler()
    transform = FunctionTransformer(np.log1p, np.expm1)
    pipeline = make_pipeline(
            transform, model
            ).fit(classfeats[['dur', 'vel', 'prev_dur', 'prev_vel']], classfeats.cls.ravel())
    
    pickle.dump(pipeline, open('etra_classifier.pickle', 'wb'))
    f['cls'] = pipeline.predict(f)
    #print(gmm)
    #eventfeats['predclass'] = gmm.predict(eventfeats[['dur', 'amp'])
    
    
    for cls, cf in f.groupby('cls'):
        cf = transform.transform(cf)
        plt.plot((cf[:,0]), (cf[:,1]), '.', label=cls, color=CLS_COLOR[cls])
        #plt.plot(cf.amp + 1.0, cf.dur + 0.1, '.', label=cls)
        #plt.plot(np.log1p(cf.amp), np.log1p(cf.amp/cf.dur), '.')
        #plt.hist(np.log(cf.amp + 0.1), bins=20, normed=True, histtype='step')
    #plt.loglog()
    #ym, xm = transform.inverse_transform(gmm.means_).T
    #xm, ym = gmm.means_.T
    #plt.plot(xm, ym, 'o')
    #plt.plot((meanspeeds[order]), gmm.means_[:,1][order], 'o')
    plt.legend()
    plt.show()

    for c, cd in classfeats.groupby('cls'):
        if c == 3: continue
        #valid = np.isfinite(cd.speed)
        plt.hist(np.log1p(cd.vel), bins=100, histtype='step', normed=True, weights=cd.weight)
        #plt.plot(np.log(cd.speed[valid] + 1.0), np.log(cd.speed), normed=True)
        #plt.plot(cd.speed)
    plt.show()

def squarehack(x): return x**2

if __name__ == '__main__': benchmark()
