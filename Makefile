#CXX=clang++-4.0
CXX=g++
DEBUG ?= 0
ifeq ($(DEBUG), 1)
	CFLAGS=-g -O0 -DEIGEN_DONT_VECTORIZE -DEIGEN_DONT_ALIGN_STATICALLY -fPIC
else
	CFLAGS=-DNDEBUG -O3 -fPIC
endif

fast_saccade_detectors.so: fast_saccade_detectors.pyx segmented_regression.hpp
	cython3 $<
	${CXX} $(CFLAGS) -I. -std=c++14 -shared `python3-config --includes` `pkg-config --cflags --libs eigen3` -o fast_saccade_detectors.so fast_saccade_detectors.c

nslr_perf: nslr_perf.cpp segmented_regression.hpp
	${CXX} `pkg-config --cflags --libs eigen3` $(CFLAGS) -std=c++14 -o $@ -Wall -g $<

.PHONY: perftest
perftest: nslr_perf
	./nslr_perf 100000


.PHONY: benchmark
benchmark: benchmark.py fast_saccade_detectors.so gazesim.py saccade_detectors.py
	./benchmark.py

.PHONY: nslr_perf.callgrind
nslr_perf.callgrind: nslr_perf
	valgrind --tool=callgrind --callgrind-out-file=$@ ./$< 100000 1.0 4

.PHONY: profile
profile: nslr_perf.callgrind
	kcachegrind $<
	
.PHONY: nslr_perf.cachegrind
nslr_perf.cachegrind: nslr_perf
	valgrind --tool=cachegrind --cachegrind-out-file=$@ ./$< 100000 1.0 4

.PHONY: profile_cache
profile_cache: nslr_perf.cachegrind
	kcachegrind $<
