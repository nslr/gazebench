import matplotlib.pyplot as plt
import numpy as np
import scipy.signal
import itertools

#DT=1/1080.0 # Global hardcoded sampling rate :(
DT=1/1009.0 # Global hardcoded sampling rate :(

def random_targets(area=(20, 20), duration=0.5, dt=DT):
    area = np.asarray(area)
    current_fixation = (np.random.uniform(size=2) - 0.5)*area
    t = 0
    while True:
        yield t, current_fixation[0], current_fixation[1]
        t += dt
        if np.random.poisson(dt/duration) > 0:
            current_fixation = (np.random.uniform(size=2) - 0.5)*area

def circular_target(radius=10.0, freq=0.5, dt=DT):
    t = 0.0
    while True:
        yield t, np.sin(t*freq)*radius, np.cos(t*freq)*radius
        t += dt

def steps(steps, dt=DT):
    next_step = 0.0
    t = 0.0
    for dur, pos in steps:
        next_step += dur
        while t < next_step:
            yield t, pos[0], pos[1]
            t += dt

def dual_step(dt=DT):
    return steps([
        (0.5, (0.0, 0.0)),
        (0.5, (5.0, 5.0)),
        (0.5, (2.5, 2.5))
        ], dt=dt)

def single_step(magnitude=(10.0, 0.0), pre_duration=0.5, post_duration=0.5, dt=DT):
    t = 0.0
    while t <= pre_duration:
        yield t, 0.0, 0.0
        t += dt

    while t < pre_duration + post_duration:
        yield t, magnitude[0], magnitude[1]
        t += dt



def random_nystagmus(area=(20, 20), speed=30.0, dt=DT):
    #pos = (np.random.uniform(size=2) - 0.5)*area
    pos = None
    target = None
    vel = None
    
    def new_target():
        nonlocal target, vel, pos
        pos = (np.random.uniform(size=2) - 0.5)*area
        target = (np.random.uniform(size=2) - 0.5)*area
        direction = target - pos
        direction /= np.linalg.norm(direction)
        vel = direction*np.random.uniform(1.0, speed)

    new_target()

    t = 0
    while True:
        yield t, pos[0], pos[1]
        t += dt
        if np.all(np.sign(vel) != np.sign(target - pos)):
            new_target()
        pos += vel*dt

    return

    current_target = (np.random.uniform(size=2) - 0.5)*area
    current_position = (np.random.uniform(size=2) - 0.5)*area
    direction = current_target - current_position
    direction /= np.linalg.norm(direction)
    while True:
        pass
    
def random_nystagmus(max_distance=[20.0, 20.0], max_duration=1.0, max_speed=30.0, dt=DT):
    #pos = (np.random.uniform(size=2) - 0.5)*area
    vel = None
    max_distance = np.array(max_distance)
    ndim = len(max_distance)
    pos = np.zeros(ndim)
    end_t = None
    def new_sp():
        nonlocal vel, pos, end_t
        direction = np.random.randn(ndim)
        direction /= np.linalg.norm(direction)
        vel = direction*np.random.uniform(0.0, max_speed)
        
        direction = np.random.randn(ndim)
        direction /= np.linalg.norm(direction)
        pos += direction*np.random.uniform(0.0, max_distance)

        end_t = t + np.random.uniform(0, max_duration)

    t = 0.0
    new_sp()
    while True:
        yield t, pos[0], pos[1]
        t += dt
        if t >= end_t:
            new_sp()
        pos += vel*dt

    return

def hz_to_nyquist(hz, sampling_rate):
    nyquist_rate = sampling_rate/2.0
    return hz/sampling_rate


# Default parameters estimated from empirical fit.
#def eyefilter(signal, sampling_rate, order=3, cutoff=33.0):
#    # TODO: Use analog version and/or better digitalization
#    # TODO: The fit doesn't seem stable, may not be a global
#    # error minimum
#    b, a = scipy.signal.bessel(order, hz_to_nyquist(cutoff, sampling_rate))
#    return scipy.signal.lfilter(b, a, signal.T).T



# TODO: Should be better
def eye_filter(targets, k=6000.0, d=90.0, v=(0.0, 0.0), pos=None):
    v = np.array(v, copy=True)
    a = np.zeros(2)
    if pos is not None:
        pos = np.array(pos, copy=True)
    prev_ts = 0.0
    for ts, x, y in targets:
        dt = ts - prev_ts
        prev_ts = ts
        if pos is None:
            pos = np.array((x, y))
            yield ts, pos[0], pos[1]
            continue
        prev_ts = ts
        a = np.subtract((x, y), pos)*k - d*v
        v += a*dt
        pos += v*dt
        yield ts, pos[0], pos[1]


def take(iterator, n):
    return list(itertools.islice(iterator, n))


def simulate_gaze(target_signal, noise=1.0):
    target = np.array(take(target_signal, 100000))
    #eye = np.array(list(eye_filter(target)))
    eye = target.copy()
    #eye[:,1:] = 
    eye = np.array(list(eye_filter(eye)))
    #x_signal = data['x'] # + np.random.randn(len(data))*0.1
    #y_signal = data['y'] #+ np.random.randn(len(data))*0.1
    data = eye.copy()
    data[:,1] += np.random.randn(len(data))*noise
    data[:,2] += np.random.randn(len(data))*noise
    data = data[::3]
    target = target[::3]
    eye = eye[::3]
    return data[:,0], data[:,1:], eye[:,1:], target[:,1:]
    #plt.plot(target[:,0], target[:,1])
    #plt.plot(eye[:,0], eye[:,1])
    #plt.show()

if __name__ == '__main__':
    target = np.array(take(random_targets(), 1000))
    eye = np.array(list(eye_filter(target)))
    data = np.fromiter(eye, [('ts', float), ('x', float), ('y', float)])
    x_signal = data['x'] # + np.random.randn(len(data))*0.1
    y_signal = data['y'] #+ np.random.randn(len(data))*0.1
    plt.plot(target[:,0], target[:,1])
    plt.plot(eye[:,0], eye[:,1])
    plt.show()
