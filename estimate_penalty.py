import pandas as pd
import sys
import matplotlib.pyplot as plt
import numpy as np

import rpy2
from rpy2.robjects import pandas2ri
pandas2ri.activate()
import rpy2.robjects as R

data = pd.DataFrame.from_csv(sys.stdin)

#data['max_speed'] += 1 # HACK!
data['penalty'] = np.exp(-(data['penalty'] + 0.01))
logit = lambda x: np.log(x/(1 - x))
delogit = lambda x: 1/(1 + np.exp(-x))

cloglog = lambda x: np.log(-np.log(1 - x))
decloglog = lambda x: 1 - np.exp(-np.exp(x))

transform = logit
untransform = delogit

data['penalty'] = transform(data['penalty'])

#data['penalty'] = logit(data['penalty'])
#print(list(data.columns.values))
pcols = list(data.columns.values)
npcols = ['penalty', 'noise', 'error', 'isnr', 'trial']
for c in npcols:
    pcols.remove(c)
best = data.groupby(pcols)['isnr'].idxmax()
best = data.loc[best]

plt.hist(best.penalty)

split_rate = 1.0/10.0
split_penalty = lambda dt: np.log(1.0 - np.exp(-split_rate*dt))

#plt.plot(np.log(1.0 - np.exp(-1.0/margin['sampling_rate'])), -margin['penalty'], color='black')
#dt = 1.0/margin['sampling_rate']
#est_penalty = split_penalty(dt)
#split_penalty_approx = lambda dt: np.log(dt*split_rate) #- dt*split_rate/2.0 + (dt*split_rate)**2/24.0
#plt.plot(dt, est_penalty)
#plt.plot(dt, split_penalty_approx(dt) - est_penalty)
#plt.show()

"""
margin = best.groupby('sampling_rate').mean().reset_index()
plt.plot((best['sampling_rate']), -best['penalty'], 'o')
plt.plot((margin['sampling_rate']), -margin['penalty'])
plt.xlabel('Sampling rate')

plt.figure()
margin = best.groupby('nystagmus_area').mean().reset_index()
plt.plot(best['nystagmus_area'], -best['penalty'], 'o')
plt.plot(margin['nystagmus_area'], -margin['penalty'])
plt.xlabel('Nystagmus area')
"""

from sklearn.linear_model import LinearRegression

import statsmodels.api as sm
import statsmodels.formula.api as smf
import betareg
X = best[pcols]
X = np.log(X)
model = sm.OLS(best['penalty'].values, sm.add_constant(X))

log = np.log
#log = lambda x: x


model = smf.quantreg('penalty ~ log(sampling_rate) + I(1/noise_level) + log(max_distance) + log(max_speed) + log(max_duration)', best)
#model = smf.probit(
#        'penalty ~ sampling_rate + noise_level + max_distance + max_speed + max_duration',
#        best)

#model = betareg.Beta.from_formula('penalty ~ sampling_rate + noise_level + max_distance + max_speed + max_duration', best)
#model = betareg.Beta(best['penalty'], best[pcols])
fit = model.fit()

# These are never implemented :(
#def betapredict(X):
#    return X

print(fit.summary())
#fit = LinearRegression().fit(X, best['penalty'])

def plot_margin(field):
    gmean = lambda x: np.expm1(np.mean(np.log1p(x)))
    margin = best.groupby(field).median().reset_index()
    #pred = fit.predict(sm.add_constant(np.log(margin[pcols]), has_constant='add'))
    pred = fit.predict(margin)
    plt.plot((best[field]), np.log(untransform(best['penalty'])), 'o')
    plt.plot(margin[field], np.log(untransform(margin['penalty'])))
    plt.plot(margin[field], np.log(untransform(pred)))
    plt.xlabel(field)
    #plt.loglog()

import scipy.stats
margin = best.groupby('noise_level').mean().reset_index()
pred_penalty = scipy.stats.norm.logpdf(margin['noise_level']*np.sqrt(2/np.pi), scale=margin['noise_level'])

for field in pcols:
    plt.figure()
    plot_margin(field)
plt.show()

def logpdf(std, value):
    std = np.array(std)
    return np.sum(value**2/(2*std**2) + np.log(1/(std*np.sqrt(2*np.pi))), axis=0)

nl = margin['noise_level']
plt.plot((best['noise_level']), -best['penalty'], 'o')
plt.plot(margin['noise_level'], -margin['penalty'])


#ests = np.array([logpdf([s, s], s*np.sqrt(2/np.pi)) for s in nl])
#plt.plot(nl, -ests, label='mad')
#ests = np.array([logpdf([s, s], s) for s in nl])
#plt.plot(nl, -ests, label='std')
#plt.legend()
#plt.semilogx()
#fit = np.poly1d(np.polyfit(np.log(margin['noise_level']), -margin['penalty'], 1))
#print(fit)
#plt.plot(np.log(margin['noise_level']), fit(np.log(margin['noise_level'])))

plt.figure()
for nl, nld in data.groupby('noise_level'):
    estp = logpdf([nl, nl], nl)
    print(nl)
    pmean = nld.groupby('penalty').mean().reset_index()
    plt.plot(pmean['penalty'], (pmean['isnr']))
    plt.axvline(2.0)
plt.xlabel('Penalty')
plt.ylabel('Isnr')

#
#plt.plot((best['noise_level']), -best['penalty'], 'o')
#plt.plot(split_penalty(margin['noise_level']), -margin['penalty'])

#plt.plot(1.0/margin['sampling_rate'], split_penalty(1.0/margin['sampling_rate']))
#plt.plot(best.nystagmus_area, best.penalty, 'o')
plt.show()
#for params, d in data.groupby(pcols):
