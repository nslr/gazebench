import numpy as np
import matplotlib.pyplot as plt
from simulate import single_step, eye_filter

amplitudes = np.linspace(1, 15, 5)
maxspeeds = []
maxaccels = []
for amplitude in np.linspace(1, 15, 5):
    target = np.array(list(single_step(magnitude=(amplitude, 0.0), pre_duration=0.1)))
    eye = np.array(list(eye_filter(target)))
    eye_speed = np.gradient(eye[:,1])/np.gradient(eye[:,0])
    #plt.plot(target[:,0], target[:,1], color='black', alpha=0.3)
    #plt.plot(eye[:,0], eye[:,1], color='black', alpha=0.3)
    maxspeeds.append(np.max(eye_speed))
plt.plot(amplitudes, maxspeeds)

plt.show()
