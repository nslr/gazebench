import numpy as np
import matplotlib.pyplot as plt
import simulate
import scipy.optimize

# From "Variability and development of a normative data base for saccadic eye movements.",
# Bahill et al 1981, Table I and Fig. 3.
MAIN_SEQUENCE = [
    #[0.0, 0.0, 0.0, 0.0],
    [5.0, 261.0, 0.042, 15000.0],
    [10.0, 410.0, 0.051, 30000.0],
    [15.0, 499.0, 0.054, 45000.0],
    [20.0, 657.0, 0.064, 60000.0],
]
MAIN_SEQUENCE = np.rec.fromrecords(MAIN_SEQUENCE, names='magnitude,peak_velocity,duration,peak_acceleration')

def model_main_sequence(k=6000.0, d=90.0):
    model = lambda target: simulate.eye_filter(target, k=k, d=d)
    seq = []
    for mag in MAIN_SEQUENCE.magnitude:
        if mag == 0:
            seq.append([0.0, 0.0, 0.0, 0.0])
        target = np.array(list(simulate.single_step(magnitude=(mag, 0.0), pre_duration=0.0)))
        eye = np.array(list(model(target)))
        #plt.plot(target[:,0], target[:,1])
        #plt.plot(eye[:,0], eye[:,1])
        velocity = np.gradient(eye[:,1])/np.gradient(eye[:,0])
        peak_velocity = np.max(velocity)
        acceleration = np.gradient(velocity)/np.gradient(eye[:,0])
        peak_acceleration = np.max(acceleration)
        saccade_dur = eye[:,0][np.argmin(velocity)]
        seq.append([mag, peak_velocity, saccade_dur, peak_acceleration])

    return np.rec.fromrecords(seq, names='magnitude,peak_velocity,duration,peak_acceleration')

model = model_main_sequence()

def error(params):
    fit = model_main_sequence(*params)
    vel_error = (fit.peak_velocity - MAIN_SEQUENCE.peak_velocity)/MAIN_SEQUENCE.peak_velocity
    accel_error = (fit.peak_velocity - MAIN_SEQUENCE.peak_velocity)/MAIN_SEQUENCE.peak_velocity
    duration_error = (fit.duration - MAIN_SEQUENCE.duration)/MAIN_SEQUENCE.duration

    return np.sum(np.abs([vel_error,
        accel_error,
        duration_error
        ]))

fit = scipy.optimize.minimize(error, [6000.0, 90.0], method='nelder-mead')

#model = model_main_sequence(*fit.x)
print(fit)

plt.subplot(3,1,1)
plt.plot(MAIN_SEQUENCE.magnitude, MAIN_SEQUENCE.peak_velocity, 'k--', alpha=0.5)
plt.plot(model.magnitude, model.peak_velocity, 'k', alpha=0.5)
plt.subplot(3,1,2)
plt.plot(MAIN_SEQUENCE.magnitude, MAIN_SEQUENCE.duration, 'k--', alpha=0.5)
plt.plot(model.magnitude, model.duration, 'k', alpha=0.5)
plt.subplot(3,1,3)
plt.plot(MAIN_SEQUENCE.magnitude, MAIN_SEQUENCE.peak_acceleration, 'k--', alpha=0.5)
plt.plot(model.magnitude, model.peak_acceleration, 'k', alpha=0.5)
plt.show()
