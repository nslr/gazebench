import numpy as np
import scipy.signal
import scipy.io
import matplotlib.pyplot as plt
import pandas as pd
import os
import cppimport
pynslr = cppimport.imp("pynslr")

#from pyparticleest.simulator import Simulator
#from pyparticleest.interfaces import ParticleFiltering
#from sklearn.mixture import GaussianMixture
#import statsmodels.api as sm

WIENER_SIZE=256

def optimal_wiener(signal, thruth, size=256):
    csd = lambda *a: scipy.signal.csd(*a, nperseg=size, window='hann')
    f, Pxx = csd(signal, signal)
    f, Psx = csd(thruth, signal)
    H = Psx/Pxx
    H = H * np.exp(-1j*2*np.pi/len(H)*np.arange(len(H))*(len(H)//2))  # shift for causal filter
    h = np.fft.irfft(H)
    return np.convolve(signal, h, mode='same')

def wiener_reconstruct(ts, sig, outliers, thruth):
    sig = sig.copy()
    for i in range(sig.shape[1]):
        sig[:,i] = optimal_wiener(sig[:,i], thruth[:,i])
    return sig

def nslr_reconstruct(ts, sig, outliers, thruth):
    #recon = pynslr.fit_gaze(ts, sig, 0.0)
    recon = pynslr.fit_gaze(ts, sig, 0.05)
    
    fit = recon(ts)
    true_noises = np.std(sig - thruth, axis=0)
    est_noises =  np.std(sig - fit, axis=0)
    print(true_noises)
    print(est_noises)
    return fit

#class ParticleModel(ParticleFiltering):
#    def __init__(self, ts, signal, outliers, thruth):
#        noisecovar = np.cov(signal - thruth, rowvar=False)
#        self.thruth = thruth
#        ndim = signal.shape[1]
#
#        speeds = np.diff(signal[~outliers], axis=0)
#        speed_model = GaussianMixture(2).fit(speeds)
#        #help(speed_model)
#        #speed_model.fit(speeds)
#        """
#        speeds[:,1] = 0
#        plt.hist(speeds[:,0], bins=100, normed=True)
#        plt.plot(speeds[:,0], np.exp(speed_model.score_samples(speeds)), '.')
#        plt.show()
#        """
#        self.noise_model = scipy.stats.multivariate_normal(np.zeros(ndim), noisecovar)
#        # TODO!
#        #self.transition_model = scipy.stats.multivariate_normal(np.zeros(ndim), noisecovar)
#        self.transition_model = speed_model
#        pass
#
#    def create_initial_estimate(self, N):
#        return np.repeat(self.thruth[:1], N, axis=0)
#
#    def sample_process_noise(self, particles, u, t):
#        p = self.transition_model.sample(len(particles))[0]
#        return p
#    
#    def update(self, particles, u, t, noise):
#        particles += noise
#
#    def measure(self, particles, y, t):
#        return self.noise_model.logpdf(particles - y)
#
#    def logp_xnext_full(self, part, past_trajs, pind,
#                        future_trajs, find, ut, yt, tt, cur_ind):
#
#        diff = future_trajs[0].pa.part[find] - part
#        return self.transition_model.score_samples(diff)
#
#def particle_filter_reconstruct(ts, sig, outliers, thruth):
#    ots = ts
#    """
#    ts = ts[::10]
#    sig = sig[::10]
#    thruth = thruth[::10]
#    outliers = outliers[::10]
#    """
#    model = ParticleModel(ts, sig, outliers, thruth)
#    #y = np.vstack((ts, sig.T)).T
#    #print(y.shape)
#    sim = Simulator(model, u=None, y=sig)
#    npart = 200
#    sim.simulate(npart, npart, smoother='mcmc')
#    est = sim.get_smoothed_mean()[1:]
#    #est = est[1:,0,0]
#    return scipy.interpolate.interp1d(ts, est, axis=0, fill_value='extrapolate')(ots) # HACK!
#
import prox_tv
def do_tv_reconstruct(l, ts, sig, outliers, thruth):
    sig = sig.copy()
    for i in range(sig.shape[1]):
        if l < 0:
            # Sometimes we get very small negative values
            # and prox_tv doesn't have it!
            assert l > -1e-6
            #print("Rounding to zero: %s"%l)
            l = 0.0
        sig[:,i] = prox_tv.tv1_1d(sig[:,i].copy(), l)
        #sig[:,i] = prox_tv.tvgen(sig[:,i].copy().reshape(-1, 1), [l, l], [1, 1], [1, 1]).reshape(-1)
    #plt.plot(sig[:,0])
    #plt.plot(thruth[:,0])
    #plt.show()

    return sig

def tv_reconstruct(ts, sig, outlier, thruth):
    valid = ~outlier
    fiterr = lambda est: np.mean(np.linalg.norm((est - thruth)[valid], axis=1)**2)
    #for l in np.linspace(0, 1000, 100):
    #    recon = do_tv_reconstruct(l, ts, sig, outlier, thruth)
    #    errs.append((l, fiterr(recon)))

    def err(l):
        recon = do_tv_reconstruct(l[0], ts, sig, outlier, thruth)
        return fiterr(recon)

    fit = scipy.optimize.minimize(err, [0.0], bounds=[(0, None)])
    #print(fit)
    l = max(0.0, fit.x[0])
    est = do_tv_reconstruct(l, ts, sig, outlier, thruth)
    """
    plt.plot(sig[:,0], '.')
    plt.plot(est[:,0])
    plt.show()
    """
    return est

import pybl
def do_bilateral_reconstruct(ps, ts, sig, outlier, thruth):
    sig = pybl.bilateral_filter1d2(ts, sig, *ps, 512, 512)
    #sig = sig.copy()
    #for dim in range(sig.shape[1]):
    #    sig[:,dim] = cv2.bilateralFilter(sig[:,dim].astype(np.float32), 9, 0.5, ps).ravel()
    return sig

def bilateral_reconstruct(ts, sig, outlier, thruth):
    valid = ~outlier
    fiterr = lambda est: np.mean(np.linalg.norm((est - thruth)[valid], axis=1)**2)

    noise = np.std(sig - thruth)
    def err(l):
        time = l[0]
        distance = 1.95*l[0]
        recon = do_bilateral_reconstruct((time, distance), ts, sig, outlier, thruth)
        err = fiterr(recon)
        return err
    fit = scipy.optimize.minimize(err, [noise], bounds=[(0, None)])
    
    #ls = np.linspace(200, 1000, 100)
    #errs = []
    #for l in ls:
    #    errs.append(err([l]))
    #plt.plot(ls, errs)
    #plt.show()
    #print(fit)
    time = fit.x[0]
    distance = 1.95*time
    return do_bilateral_reconstruct((time, distance), ts, sig, outlier, thruth)

def scanpathplot(ts, xs, recon, thruth, outliers):
    plt.figure(1, figsize=(12, 6))
    tx_ax = plt.subplot2grid((2,2), (0, 0))
    tx_ax.set_ylabel("Horiz. gaze (degrees)")
    #tx_ax.set_xlabel("Time (seconds)")
    ty_ax = plt.subplot2grid((2,2), (1, 0), sharex=tx_ax)
    ty_ax.set_ylabel("Vert. gaze (degrees)")
    ty_ax.set_xlabel("Time (seconds)")
    xy_ax = plt.subplot2grid((2,2), (0, 1), rowspan=2)
    xy_ax.set_xlabel("Horiz. gaze (degrees)")
    xy_ax.set_ylabel("Vert. gaze (degrees)")
    labels = []
    txplots = []
    def addtxplot(t, plots):
        for artist in plots:
            artist._origdatahack = artist.get_data()
            artist._timehack = np.array(t)
            txplots.append(artist)

    #for c, cd in session.groupby('cls'):
    #    tx_ax.plot(cd.ts, cd.x, 'o', color=CLASS_COLORS[c])
    #    ty_ax.plot(cd.ts, cd.y, 'o', color=CLASS_COLORS[c])
    #    addtxplot(cd.ts, xy_ax.plot(cd.x, cd.y, 'o', color=CLASS_COLORS[c]))
    #    if c in INCLUDE_CLASSES:
    #        labels.append((txplots[-1], "%s (human)"%(CLASSES[c])))
    
    thruth = thruth.copy()
    thruth[outliers] = np.nan
    xs = xs.copy()
    xs[outliers] = np.nan
    recon = recon.copy()
    recon[outliers] = np.nan


    tx_ax.plot(ts, thruth[:,0], '-', color='black')
    ty_ax.plot(ts, thruth[:,1], '-', color='black')
    addtxplot(ts, xy_ax.plot(thruth[:,0], thruth[:,1], '-', color='black'))
    labels.append((txplots[-1], "Measurement"))
    
    """
    tx_ax.plot(ts, xs[:,0], '.', color='black', alpha=0.1)
    ty_ax.plot(ts, xs[:,1], '.', color='black', alpha=0.1)
    addtxplot(ts, xy_ax.plot(xs[:,0], xs[:,1], '.', color='black', alpha=0.1))
    labels.append((txplots[-1], "Measurement"))
    """
    
    """
    tx_ax.plot(ts, recon[:,0], '-', color='red')
    ty_ax.plot(ts, recon[:,1], '-', color='red')
    addtxplot(ts, xy_ax.plot(recon[:,0], recon[:,1], '-', color='red'))
    labels.append((txplots[-1], "NSLR"))
    """
    
    plt.gcf().legend(*zip(*labels), 'upper center', ncol=3)
    def hide_stuff(ax):
        lim = ax.get_xlim()
        for artist in txplots:
            odata = artist._origdatahack
            t = artist._timehack
            include = (t >= lim[0]) & (t <= lim[1])
            artist.set_data(odata[0][include], odata[1][include])


        xy_ax.relim()
        xy_ax.autoscale_view()
        
        view_d = thruth[(ts >= lim[0]) & (ts <= lim[1])]
        tx_ax.set_ylim(np.nanmin(view_d[:,0]), np.nanmax(view_d[:,0]))
        ty_ax.set_ylim(np.nanmin(view_d[:,1]), np.nanmax(view_d[:,1]))

        plt.draw()
    tx_ax.callbacks.connect('xlim_changed', hide_stuff)
    ty_ax.callbacks.connect('xlim_changed', hide_stuff)

    #tx_ax.set_xlim(3.5, 7.5)
    #tx_ax.set_xlim(18, 23)
 
    plt.show()
    #plt.gcf().show()



algorithms = {
    'NSLR': nslr_reconstruct,
    'Wiener': wiener_reconstruct,
    'Total Variation': tv_reconstruct,
    #'Bilateral': bilateral_reconstruct,
    #'pfilt': particle_filter_reconstruct, # Too slow
}

def isnrs(ts, xs, outliers, algorithms=algorithms, noise_levels=np.linspace(0.03, 3, 30)):
    result = pd.DataFrame([], columns="algorithm,noise_level,noise,error,isnr".split(','))

    xs = scipy.interpolate.interp1d(ts[~outliers], xs[~outliers], axis=0, fill_value='extrapolate')(ts)
    include = ~outliers
    include[:WIENER_SIZE] = False
    include[-WIENER_SIZE:] = False
    do_plot = False
    fiterr = lambda est: np.mean(np.linalg.norm((est - xs)[include], axis=1)**2)
    noise_levels = [0.1, 0.5, 1.5]
    noise_levels = [0.0]
    for i, noise_level in enumerate(noise_levels):
        sig = xs + np.random.randn(*xs.shape)*noise_level
        noise = fiterr(sig)
        for name, algo in algorithms.items():
            recon = algo(ts, sig, outliers, xs)
            error = fiterr(recon)
            isnr = 10*np.log10(noise/error)
            result = result.append(dict(
                algorithm=name,
                noise_level=noise_level,
                noise=noise,
                error=error,
                isnr=isnr
                ), ignore_index=True)
            print(result.iloc[-1])
            if name == 'NSLR':
                diffs = recon - xs
                #plt.hist(np.abs(diffs[:,0]), bins=100)
                print("XSTD", np.std(diffs[:,0]), np.std(diffs[:,1]))
                plt.show()
                scanpathplot(ts, sig, recon, xs, outliers)
                """
                plt.subplot(len(noise_levels), 1, i+1)
                print(i)
                my = (ts >= 18) & (ts <= 23)
                sp = plt.plot(ts[my], sig[my,0], '.', color='black', alpha=0.1)
                xp = plt.plot(ts[my], xs[my,0], '-', color='black', alpha=0.7)
                rp = plt.plot(ts[my], recon[my,0], '-', color='red')
                plt.ylim(np.min(xs[my,0]), np.max(xs[my,0]))
                plt.xlim(18, 23)
                if i == 1:
                    plt.ylabel('Horiz. gaze (degrees)')
                if i == len(noise_levels) - 1:
                    plt.xlabel('Time (seconds)')
                    plt.gcf().legend(
                        [xp[0], sp[0], rp[0]],
                        ['Gaze', 'Measurement', 'NSLR'],
                        'upper center', ncol=3,
                        )
                    plt.show()
                else:
                    plt.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
                """
            if do_plot:
                ax = plt.subplot(2,1,1)
                plt.plot(ts[include], recon[include,0], label=name, alpha=0.8, **ALGOSTYLES[name])
                #plt.subplot(2,1,2, sharex=ax, sharey=ax)
                #if name == 'NSLR':
                #    plt.plot(ts[include], recon[include,0], label=name, alpha=0.8, **ALGOSTYLES[name])
        if do_plot:
            ax = plt.subplot(2,1,1)
            plt.plot(ts[include], xs[include,0], color='black', alpha=0.8, label='Ground thruth')
            plt.plot(ts, sig[:,0], 'k.', alpha=0.1, label='Simulated signal')
            plt.xlabel('Time (seconds)')
            plt.ylabel('Horiz. gaze position ($\circ$)')
            plt.legend(loc=3, mode='expand', ncol=3, bbox_to_anchor=(0., 1.02, 1., .102), borderaxespad=0)
            """
            plt.subplot(2,1,2, sharex=ax, sharey=ax)
            plt.xlabel('Time (seconds)')
            plt.ylabel('Horiz. gaze position ($\circ$)')
            plt.plot(ts[include], xs[include,0], color='black', alpha=0.8, label='Ground thruth')
            plt.plot(ts, sig[:,0], 'k.', alpha=0.1, label='Simulated signal')
            plt.legend()
            """
            plt.show()
    return result


def get_texas():
    path = 'ref/classificator_1.5/Pursuit_2D/1st/'
    for f in os.listdir(path):
        f = os.path.join(path, f)
        d = pd.read_csv(f, delimiter='\t')
        #d = d.iloc[::10]
        ts = d.TimeStamp.values/1000.0
        xs = d[['X_Position_Degree_Left_Eye', 'Y_Position_Degree_Left_Eye']].values
        outliers = d['Validity_Left_Eye'].values != 0
        yield ts, xs, outliers

def benchmark_texas():
    path = 'ref/classificator_1.5/Pursuit_2D/1st/'
    results = []
    def getem():
        for f in os.listdir(path):
            f = os.path.join(path, f)
            d = pd.read_csv(f, delimiter='\t')
            #d = d.iloc[::30]
            ts = d.TimeStamp.values/1000.0
            xs = d[['X_Position_Degree_Left_Eye', 'Y_Position_Degree_Left_Eye']].values
            outliers = d['Validity_Left_Eye'].values != 0
            yield ts, xs, outliers
            #results.append(isnrs(ts, xs.copy(), outliers))
    do_benchmark(getem())

    """
    results = pd.concat(results, ignore_index=True)
    results = results.groupby(['noise_level', 'algorithm']).mean().reset_index()
    for algo, ad in results.groupby('algorithm'):
        #plt.plot(ad.noise_level, ad.isnr, label=algo)
        plt.plot(ad.noise_level, np.sqrt(ad.error)/np.sqrt(2), label=algo)
    plt.legend()
    plt.show()
    """

from scipy.stats.mstats import gmean

ALGOSTYLES = {
    'NSLR': dict(linestyle='-', color='r'),
    'Total Variation': dict(linestyle='--', color='b'),
    'Wiener': dict(linestyle=':', color='g'),
        }

from itertools import islice
import random
def benchmark():
    np.random.seed(0)
    random.seed(0)
    #sessions = islice(get_hollywood(), 50)
    #sessions = islice(get_hollywood2(), 2, 100)
    sessions = islice(get_simulated(), 100)
    #sessions = get_texas()
    #sessions = get_lund()
    results = []
    for ts, xs, outliers in sessions:
        xs = scipy.interpolate.interp1d(ts[~outliers], xs[~outliers], axis=0, fill_value='extrapolate')(ts)
        results.append(isnrs(ts, xs.copy(), outliers))
    
    results = pd.concat(results, ignore_index=True)
    results = results.groupby(['noise_level', 'algorithm']).mean().reset_index()
    for algo, ad in results.groupby('algorithm'):
        plt.plot(ad.noise_level, ad.isnr, label=algo, **ALGOSTYLES[algo])
        #plt.plot(ad.noise_level, np.sqrt(ad.error)/np.sqrt(2), label=algo)
    plt.legend()
    #plt.title("Synthetic")
    plt.xlabel('Simulated noise standard deviation (degrees)')
    plt.ylabel('Improvement in Signal-to-Noise Ratio (decibels)')
    plt.show()

from glob import glob
def get_hollywood(min_dur=20):
    sessions = []#pd.DataFrame([], columns=['subject','clip','path'])

    geom = pd.read_csv("datasets/hollywood/geometry.txt", delimiter='\t',
            names='distance,width,height,xres,yres'.split(','))
    def pixels_to_angles(xs):
        screenmeters = xs/geom[['xres', 'yres']].values
        screenmeters -= 0.5
        screenmeters *= geom[['width', 'height']].values
        angles = np.degrees(np.arctan(screenmeters/geom.distance.values))
        #plt.subplot(2,1,1)
        #plt.plot(screenmeters[:,0])
        #plt.subplot(2,1,2)
        #plt.plot(angles[:,0])
        #plt.show()
        #print(screenmeters)
        return angles
    
    for p in sorted(glob("datasets/hollywood/samples/*.txt")):
        b = os.path.splitext(os.path.basename(p))[0]
        subject, clip = b.split('_')
        sessions.append((subject, clip, p))
    sessions = pd.DataFrame.from_records(sessions, columns='subject,clip,path'.split(','))
    sessions = sessions.sample(frac=1)
    for _, s in sessions.iterrows():
        f = s.path
        d = pd.read_csv(f, names="ts,diameter,area,x,y,event".split(','), delimiter='\t', index_col=False)
        outliers = d.event == 'B'
        ts = d.ts.values/1e6
        if len(ts) == 0:
            continue
        dur = ts[-1]
        if dur < min_dur:
            continue
        xs = pixels_to_angles(d[['x', 'y']].values)
        yield ts, xs, outliers

from zipfile import ZipFile
def get_hollywood2(min_dur=20):
    sessions = []
    for subjdir in glob("datasets/cox_hollywood/subject?.zip"):
        d = ZipFile(subjdir)
        for f in d.namelist():
            if f.startswith('__'): continue
            if not f.endswith('.coord'): continue
            sessions.append(lambda d=d, f=f: d.open(f))
    random.shuffle(sessions)
    for f in sessions:
        f = f()
        while True:
            line = next(f)
            if not line.startswith(b'#'):
                break
        resline = line
        resolution = np.array([float(c) for c in resline.split()[1:]])
        geomline = next(f).split()
        distance, width, height = [float(geomline[i]) for i in (2, 4, 6)]
        size = np.array([width, height])
        def pixels_to_angles(xs):
            screenmeters = xs/resolution
            screenmeters -= 0.5
            screenmeters *= size
            angles = np.degrees(np.arctan(screenmeters/distance))
            #plt.subplot(2,1,1)
            #plt.plot(screenmeters[:,0])
            #plt.subplot(2,1,2)
            #plt.plot(angles[:,0])
            #plt.show()
            #print(screenmeters)
            return angles
        f = pd.read_csv(f, delimiter=' ', header=None,
                names='ts,x,y,confidence'.split(','),
                dtype=float
                #dtype={'ts': int, 'x': float, 'y': float, 'status': int}
                )
        f.ts /= 1e6
        if f.ts.iloc[-1] < min_dur: continue
        
        #f = f[::10]
        yield f.ts, pixels_to_angles(f[['x', 'y']]).values, f.confidence < 1
        #print(f.decode('utf-8'))

    yield None

import simulate
def get_simulated():
    while True:
        target = simulate.random_nystagmus()
        eye = simulate.eye_filter(target)
        sampling_rate = 200
        eye = np.array(list(islice(eye, int(20000))))
        eye_interp = scipy.interpolate.interp1d(eye[:,0], eye, axis=0)
        new_ts = np.arange(0, eye[:,0][-1], 1.0/sampling_rate)
        #eye = eye_interp(new_ts)
        yield eye[:,0], eye[:,1:], np.zeros(len(eye[:,0]), dtype=np.bool)

def angleizer(dist, w, h, resx, resy):
    def pixels_to_angles(xs):
        screenmeters = xs/np.array([resx, resy])
        screenmeters -= 0.5
        screenmeters *= np.array([w, h])
        angles = np.degrees(np.arctan(screenmeters/dist))
        #plt.subplot(2,1,1)
        #plt.plot(screenmeters[:,0])
        #plt.subplot(2,1,2)
        #plt.plot(angles[:,0])
        #plt.show()
        #print(screenmeters)
        return angles
    return pixels_to_angles

def get_lund():
    basedir = 'ref/EyeMovementDetectorEvaluation'
    def read_sessions():
        sessions = pd.read_csv('ref/EyeMovementDetectorEvaluation/sessions.csv')
        files = glob(os.path.join(basedir, 'annotated_data', '*', '*.mat'))
        for path in files:
            yield scipy.io.loadmat(path)

    for d in read_sessions():
        w, h = d['ETdata']['screenDim'][0][0][0]
        dist = d['ETdata']['viewDist'][0][0][0]
        resx, resy = d['ETdata']['screenRes'][0][0][0]
        pixels_to_angles = angleizer(dist, w, h, resx, resy)
        dt = 1.0/int(d['ETdata']['sampFreq'])
        ed = pd.DataFrame(d['ETdata']['pos'][0][0], columns='ts,pupil_w,pupil_h,x,y,cls'.split(','))
        #ed.cls[ed.cls == 4] = 1
        #ed.cls[ed.cls == 3] = 1
        xs = pixels_to_angles(ed[['x', 'y']].values)
        ts = np.arange(0, len(ed))*dt
        outliers = ed.cls > 4
        yield ts, xs, outliers.values


if __name__ == '__main__':
    benchmark()


