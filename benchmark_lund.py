import scipy.io
import scipy.stats
from pprint import pprint
import matplotlib.pyplot as plt
from glob import glob
import os
import pandas as pd
import numpy as np
import cppimport
import sys
import itertools
from collections import defaultdict
import segment_classifier
pynslr = cppimport.imp('pynslr')

CLASSES = {
    1: 'Fixation',
    2: 'Saccade',
    3: 'PSO',
    4: 'Smooth pursuit',
    5: 'blink',
    6: 'undefined',
    22: 'saccade cont.',
    11: 'fix cont.',
    44: 'pursuit cont.'
}

CLASS_COLORS = {
    1: 'b',
    2: 'k',
    3: 'y',
    4: 'g',
    5: 'm',
    6: 'c',
    22: 'r',
    11: 'b',
    44: 'g'
}

def angleizer(dist, w, h, resx, resy):
    def pixels_to_angles(xs):
        screenmeters = xs/np.array([resx, resy])
        screenmeters -= 0.5
        screenmeters *= np.array([w, h])
        angles = np.degrees(np.arctan(screenmeters/dist))
        #plt.subplot(2,1,1)
        #plt.plot(screenmeters[:,0])
        #plt.subplot(2,1,2)
        #plt.plot(angles[:,0])
        #plt.show()
        #print(screenmeters)
        return angles
    return pixels_to_angles

import networkx as nx
from fractions import Fraction
def plot_hmm(transitions, labels=None):
    g = nx.DiGraph()
    N = len(transitions)
    if labels is None:
        labels = list(range(N))
    for start, end in itertools.product(range(N), repeat=2):
        p = transitions[start][end]
        if p == 0: continue
        g.add_edge(labels[start], labels[end], weight=p, label="%.2f"%p)
    from networkx.drawing.nx_agraph import write_dot
    write_dot(g, 'hmm.dot')
    #nx.draw_graphviz(g, with_labels=True)
    #plt.show()

import re
meta_pattern = r'.*/(?P<stimtype>.*)/(?P<sinit>..)(?P<subject>..)_(?P<stimulus>.*)_labelled_(?P<annotator>..)\.mat'
meta_base_pattern = r'(?P<sinit>..)(?P<subject>..)_(?P<stimulus>.*)_labelled_(?P<annotator>..)\.mat'

basedir = 'ref/EyeMovementDetectorEvaluation'

def load_classification():
    others = os.path.join(basedir, "matlab_analysis_code/20150807.mat")
    others = scipy.io.loadmat(others)
    algos = others['nameOfAlgorithm']
    algos = [algo[0] for algo in algos[0]]
    fnames = [fname[0] for fname in others['fnames'][0]]
    events = others['events'][0]
    data = []
    recordings = []
    for i, fname in enumerate(fnames):
        meta = re.match(meta_base_pattern, fname)
        meta = meta.groupdict()
        recordings.append("%s/%s"%(meta['subject'], meta['stimulus']))
        es = events[i]
        es = {name: es[name].ravel() for name in es.dtype.names}
        data.append(pd.DataFrame.from_records(es))
        #print(data[-1])
    data = pd.concat(data, keys=recordings)
    return data
references = load_classification()

def read_sessions():
    sessions = pd.read_csv('ref/EyeMovementDetectorEvaluation/sessions.csv')
    for _, session in sessions.iterrows():
        files = os.path.join(basedir, 'annotated_data', '*', "??%02i_*%s_labelled*"%(session.subject, session.stimulus))
        files = glob(files)
        if len(files) != 2:
            continue
        for f in files:
            meta = re.match(meta_pattern, f)
            meta = meta.groupdict()
            meta['recording'] = "%s/%s"%(meta['subject'], meta['stimulus'])
            yield meta, scipy.io.loadmat(f)
            #print(meta)

#for f in glob("ref/EyeMovementDetectorEvaluation/annotated_data/*/*"):
#    #name = os.path.basename(f)
#    meta = re.match(meta_pattern, f)
#    print(meta.groupdict())
#    #print(meta_pattern)
#    #print(f)
#    #print(meta_pattern)
#    #print(f)

classfeats = []#pd.DataFrame([], columns='duration,velocity,cls')

transitions = defaultdict(lambda: defaultdict(int))
total_durations = defaultdict(int)
initial_states = np.zeros(len(CLASSES))
sessions = []
for i, (meta, d) in enumerate(read_sessions()):
    #if meta['stimtype'] == 'dots': continue
    #if meta['annotator'] != 'RA': continue
    #if meta['annotator'] != 'MN': continue
    w, h = d['ETdata']['screenDim'][0][0][0]
    dist = d['ETdata']['viewDist'][0][0][0]
    resx, resy = d['ETdata']['screenRes'][0][0][0]
    pixels_to_angles = angleizer(dist, w, h, resx, resy)
    dt = 1.0/int(d['ETdata']['sampFreq'])
    ed = pd.DataFrame(d['ETdata']['pos'][0][0], columns='ts,pupil_w,pupil_h,x,y,cls'.split(','))
    #ed.cls[ed.cls == 4] = 1
    #ed.cls[ed.cls == 3] = 1
    ed[['x', 'y']] = pixels_to_angles(ed[['x', 'y']].values)
    #ed.x += np.random.randn(len(ed))*1.0
    #ed.y += np.random.randn(len(ed))*1.0
    ed.ts = np.arange(0, len(ed))*dt
    for k, v in meta.items():
        ed[k] = v
    sessions.append((meta, ed))

    est = pynslr.fit_gaze(ed.ts, ed[['x', 'y']])
    ax = plt.subplot(2,1,1)
    plt.subplot(2,1,2, sharex=ax)
    prev_duration = 0.1
    prev_velocity = 1.0
    prev_speed = np.array([[0.0, 0.0]])
    prev_cls = 1
    prev_amplitude = 0
    prev_direction = np.array([[0.0, 0.0]])
    for seg in est.segments:
        slc = slice(*seg.i)
        #sd = ed.iloc[slc]
        cls = scipy.stats.mode(ed.cls.values[slc]).mode[0]
        inclass = ed.cls.values[slc] == cls
        cls_certainty = np.sum(inclass)/len(inclass)
        duration = float(np.diff(seg.t))
        duration += np.random.uniform(-dt/2, dt/2)
        speed = np.diff(seg.x, axis=0)/duration
        direction = speed/np.linalg.norm(speed)
        velocity = np.linalg.norm(speed)
        #speed_diff = float(np.dot(speed, prev_speed.T))
        speed_diff = float(np.linalg.norm(speed - prev_speed))
        direction_diff = float(np.dot(prev_direction, direction.T))
        if direction_diff != direction_diff:
            direction_diff = 0
        
        # Scale for beta distribution
        #direction_diff += 1.0
        #direction_diff /= 2.0
        # Fisher transform
        direction_diff *= (1 - 1e-6) # Avoid -1 and 1
        direction_diff = np.arctanh(direction_diff)
        #print(direction_diff)
        amplitude = velocity*duration
        """
        if cls == 2 and (prev_cls == 2 or prev_cls == 22):
            cls = 22
        if cls == 1 and (prev_cls == 1 or prev_cls == 11):
            cls = 11
        if cls == 4 and (prev_cls == 4 or prev_cls == 44):
            cls = 44
        """

        #print(velocity)
        classfeats.append(dict(
            duration=duration,
            velocity=velocity,
            speed_diff=speed_diff,
            direction_diff=direction_diff,
            prev_velocity=prev_velocity,
            prev_duration=prev_duration,
            amplitude=amplitude,
            prev_amplitude=prev_amplitude,
            session=i,
            prev_cls=prev_cls,
            cls_certainty=cls_certainty,
            annotator=meta['annotator'],
            ts=seg.t[0],
            recording=meta['recording'],
            cls=cls))
        
        transitions[prev_cls][cls] += 1.0
        #durations[int(cls - 1)] += duration
        total_durations[cls] += duration
        #if seg.i[0] == 0:
        #    initial_states[int(cls - 1)] += 1.0

        prev_duration = duration
        prev_velocity = velocity
        prev_speed = speed
        prev_cls = cls
        prev_amplitude = amplitude
        prev_direction = direction
        continue
        plt.subplot(2,1,1)
        plt.plot(seg.t, np.array(seg.x)[:,0], 'o-', color='black')#color=CLASS_COLORS[cls])
        plt.subplot(2,1,2, sharex=ax)
        plt.plot(seg.t, np.array(seg.x)[:,1], 'o-', color='black')#, color=CLASS_COLORS[cls])
    continue
    ax = plt.subplot(2,1,1)
    for cls, cd in ed.groupby('cls'):
        plt.plot(cd.ts, cd.x, '.', label=CLASSES[cls], color=CLASS_COLORS[cls])
    
    plt.subplot(2,1,2, sharex=ax)
    for cls, cd in ed.groupby('cls'):
        plt.plot(cd.ts, cd.y, '.', label=CLASSES[cls], color=CLASS_COLORS[cls])
    plt.legend()
    plt.show()
    break
    #print(ed)

classfeats = pd.DataFrame.from_records(classfeats)
#classfeats = classfeats.drop('session', axis=1)
#classfeats = classfeats[classfeats.cls <= 4]
classfeats = classfeats.copy()
INCLUDE_CLASSES = [1, 2, 3, 4]
n_classes = len(INCLUDE_CLASSES)
tmatrix = np.zeros([n_classes]*2)
#transitions = transitions[:len(INCLUDE_CLASSES),:len(INCLUDE_CLASSES)]
transitions = {
        f: {t: transitions[f][t] for t in INCLUDE_CLASSES}
        for f in INCLUDE_CLASSES
        }
for i, start in enumerate(INCLUDE_CLASSES):
    for j, end in enumerate(INCLUDE_CLASSES):
        tmatrix[i,j] = transitions[start][end]
transitions = tmatrix

transitions = np.ones((n_classes, n_classes))
transitions[0, 2] = 0
transitions[3, 2] = 0
transitions[2, 1] = 0
transitions[3, 0] = 0.5
transitions[0, 3] = 0.5

print(transitions)
for i in range(len(transitions)):
    transitions[i] /= np.sum(transitions[i])
plt.clf()
plot_hmm(transitions, [CLASSES[cls] for cls in INCLUDE_CLASSES])

"""
transition_rates = transitions.copy()
diag = np.diag_indices(len(transition_rates))
transition_rates[diag] = 0
transition_rates /= np.sum(transition_rates, axis=1).reshape(-1, 1)
state_durations = np.array([0.3, 0.05, 0.1, 0.3])
state_rates = 1/state_durations
transition_rates *= state_rates.reshape(-1, 1)
transition_rates[diag] = -state_rates
"""
#print(transition_rates)
#print(transition_rates/state_durations.reshape(-1, 1))

#transitions[3, 2] = 0
#transitions[:, 3] = 0
#transitions[3, 3] = 1
#transitions[2, 3] = 1
#transitions[3, 2] = 0
#transitions[0, 2] = 0

stationary = transitions
for i in range(10):
    stationary = np.dot(stationary, stationary)
initial = stationary[0]
vals, vecs = np.linalg.eig(transitions.T)
#print(vecs[0])
#print(np.sum(transitions[0]))
#print(transitions[0])
#print(transitions)
for row in transitions:
    for c in row:
        oc = c
        #c = Fraction(c).limit_denominator(100)
        #assert (c - oc) < 1e-6
        #sys.stdout.write(str(c) + '\t')
        sys.stdout.write("%.2f"%c + '\t')
    print()

plt.clf()


from sklearn.manifold import TSNE, MDS
from sklearn.decomposition import PCA, FastICA
from sklearn.covariance import MinCovDet

class Gclass:
    @classmethod
    def Estimate(mycls, feats, classes):
        class_labels = sorted(np.unique(classes))
        #self.means = []
        #self.covars = []
        dists = {}
        for cls in class_labels:
            cf = feats[classes == cls]
            robust = MinCovDet().fit(cf)
            mean = robust.location_
            covar = robust.covariance_
            covar = np.diag(np.diag(covar))
            #mean = np.mean(cf, axis=0)
            #covar = np.cov(cf, rowvar=False)
            #covar = np.diag(np.diag(covar))
            #print(cls, ':', [mean.tolist(), (covar).tolist()])
            dists[cls] = scipy.stats.multivariate_normal(mean, covar)
        return mycls(dists)
    
    def __init__(self, dists):
        self.classidx = {}
        self.idxclass = []
        self.dists = []
        for i, (cls, dist) in enumerate(dists.items()):
            self.idxclass.append(cls)
            self.classidx[cls] = i
            self.dists.append(dist)
        self.idxclass = np.array(self.idxclass)
    
    def liks(self, d):
        scores = []
        scores = [dist.pdf(d) for dist in self.dists]
        return np.array(scores).T
    
    def classify(self, d):
        return np.argmax(self.liks(d), axis=1)

    def dist(self, cls):
        return self.dists[self.classidx[cls]]

#plt.plot(np.log(classfeats['amplitude'] + 1e-3), np.log(classfeats['duration']), '.')
#plt.show()

"""
cf = classfeats[['velocity', 'speed_diff']]
cf.velocity = np.log(cf.velocity + 1e-6)
cf.speed_diff = (np.log(cf.speed_diff + 1e-6) - np.log(classfeats.velocity + classfeats.prev_velocity + 1e-6))
"""



#cf = classfeats[['amplitude', 'duration']].copy()
#cf.amplitude += 0.1
#cf = np.log(cf)

# Use only segments where both annotators
# agree
annotators, ad = zip(*classfeats.groupby('annotator'))
ad = [d.set_index(['recording', 'ts']) for d in ad]
basedata = ad[0].drop(['cls', 'annotator'], axis=1)
cls = pd.concat([d['cls'] for d in ad], axis=1, keys=annotators)
classfeats = pd.concat([basedata, cls], axis=1)
classfeats = classfeats[classfeats.MN == classfeats.RA]
classfeats['cls'] = classfeats.MN

"""
cf = classfeats[['velocity', 'speed_diff']]
cf.velocity = np.log(cf.velocity + 1e-6)
cf.speed_diff = np.log(cf.speed_diff + 1e-6)
#cf.duration = np.log(cf.duration)
"""
"""
cf = classfeats[['velocity', 'direction_diff']]
cf['velocity'] = np.log(cf.velocity + 1e-6)
"""

def safelog(x):
    return np.log10(np.clip(x, 1e-6, None))

cf = classfeats[['velocity', 'direction_diff']]
cf.velocity = safelog(cf.velocity)
#cf.duration = safelog(cf.duration)

#cf = np.log(classfeats[['velocity']] + 1e-6).copy()

#cf.velocity = np.log1p(cf.velocity)
#cf = np.log1p(cf)
pca = PCA().fit(cf)
#cf = pca.transform(cf)
cf = cf.values
include = classfeats.cls.isin(INCLUDE_CLASSES)
model = Gclass.Estimate(cf[include], classfeats.cls[include])

"""
classparams = {
1: [[1.815959837674831, 0.1058750630806106], [[1.418096955960837, 0.9359854038883433], [0.9359854038883433, 1.15155591420667]]],
4: [[5.60887517236423, 0.04489760200655303], [[1.2423014407843886, 0.9003947961078382], [0.9003947961078382, 1.163256806138306]]],
3: [[42.94527999326823, 0.011308114233143202], [[1.3366921277346238, 0.9299405142637657], [0.9299405142637657, 1.095066102771153]]],
2: [[210.3690256423827, 0.0077705738771196116], [[1.1381122881889292, 0.983426315361128], [0.983426315361128, 1.1625372500648354]]],
}

classparams = {
2 : [[175.36267892104286, 0.008791148672164732], [[1.1680852973583806, 0.9801906376321168], [0.9801906376321168, 1.1052132545892814]]] ,
3 : [[36.140449808231516, 0.012840141814969891], [[1.2685409789919146, 0.9312910677600725], [0.9312910677600725, 1.0820709371266828]]] ,
4 : [[5.217671161348852, 0.057008493503891346], [[1.2235125495119668, 0.9134899557349075], [0.9134899557349075, 1.179174595403464]]] ,
1 : [[1.572996794955357, 0.12824897976294802], [[1.399493534074105, 0.9394522480845002], [0.9394522480845002, 1.1110711516106655]]] ,
}

classparams = {cls: (np.log10(m), np.log10(c)) for cls, (m, c) in classparams.items()}

mdiff = ((classparams[4][0][1] - classparams[1][0][1])/2)**2
classparams[1][0][1] = classparams[4][0][1] = (classparams[4][0][1] + classparams[1][0][1])/2.0
classparams[1][1][1][1] += mdiff
classparams[4][1][1][1] += mdiff

classparams = {
    1: [[0.16561825633246283, -0.84552913744447, -0.06630034631762113], [0.12850128537312733, 0.03734509272650963, 2.5868548741765034]],
    2: [[2.242717636210864, -2.0847617595248775, 1.1670825620755678], [0.06496430301248143, 0.03523858252433229, 2.1943176425238375]],
    3: [[1.6247636213447365, -1.8816720172030845, -1.2764920043439774], [0.117615175339527, 0.021530593157486816, 3.0270378300198386]],
    4: [[0.7416937461371016, -1.276775686953193, -0.32787468726264335], [0.08370839726780385, 0.04713689901170634, 3.1640099606221015]]
}

mdiff = ((classparams[4][0][1] - classparams[1][0][1])/2)**2
classparams[1][0][1] = classparams[4][0][1] = (classparams[4][0][1] + classparams[1][0][1])/2.0
classparams[1][1][1] += mdiff
classparams[4][1][1] += mdiff

classparams = {
1.0 : [[0.09985778567918241, -0.2122496537687585], [[0.07856828470208563, -0.002190091863449239], [-0.002190091863449239, 1.467024448165057]]],
2.0 : [[2.3965006391002524, 0.40333913059605025], [[0.07572530069512588, -0.00034456205664450297], [-0.00034456205664450297, 2.8486611658942707]]],
3.0 : [[1.6426370515307516, -1.027679029252761], [[0.07572925272656668, -0.11509645175182336], [-0.11509645175182336, 1.6361327098134113]]],
4.0 : [[0.7782569324147526, -0.1537361542240595], [[0.08435367157067372, -0.013130749526710407], [-0.013130749526710407, 2.838279123043138]]],
}

#= classparams[4][0][1] = (classparams[4][0][1] + classparams[1][0][1])/2.0

dists = {
        cls: scipy.stats.multivariate_normal(m, c)
        for cls, (m, c) in classparams.items()
        }
model = Gclass(dists)
"""

def viterbi(initial_probs, transition_probs, emissions):
    n_states = len(initial_probs)
    emissions = iter(emissions)
    emission = next(emissions)
    transition_probs = safelog(transition_probs)
    probs = safelog(emission) + safelog(initial_probs)
    stack = []
    
    for emission in emissions:
        emission /= np.sum(emission)
        trans_probs = transition_probs + np.row_stack(probs)
        max_col_ixs = np.argmax(trans_probs, axis=0)
        probs = safelog(emission) + trans_probs[max_col_ixs, np.arange(n_states)]
        stack.append(max_col_ixs)
    state_seq = [np.argmax(probs)]

    while stack:
        max_col_ixs = stack.pop()
        state_seq.append(max_col_ixs[state_seq[-1]])

    state_seq.reverse()

    return state_seq

def viterbi_semi_ct(initial_probs, transition_rates, emissions):
    n_states = len(initial_probs)
    emissions = iter(emissions)
    pt, emission = next(emissions)
    probs = emission*initial_probs
    stack = []
    state_idx = range(n_states)
    self_rates = -transition_rates[np.diag_indices(n_states)]
    other_probs = transition_rates/self_rates.reshape(-1, 1)
    other_probs[np.diag_indices(n_states)] = 0
    #print(other_probs)

    for t, emission in emissions:
        dt = t - pt
        pt = t
        trans_probs = np.zeros((n_states, n_states))
        for start in range(n_states):
            myrate = self_rates[start]
            self_prob = np.exp(-dt*myrate)
            leftover = 1 - self_prob
            trans_probs[start, start] = self_prob
            for end in range(n_states):
                if start == end: continue
                trans_probs[start, end] = other_probs[start, end]*leftover

        #trans_probs = np.exp(transition_rates*dt)*np.row_stack(probs)
        trans_probs *= np.row_stack(probs)
        most_likely_transitions = np.argmax(trans_probs, axis=0)
        probs = emission*trans_probs[most_likely_transitions, state_idx]
        probs /= np.sum(probs)
        stack.append(most_likely_transitions)
    state_seq = [np.argmax(probs)]
    while stack:
        most_likely_transitions = stack.pop()
        state_seq.append(most_likely_transitions[state_seq[-1]])
    state_seq.reverse()
    return state_seq

def viterbi_ct(initial_probs, transition_rates, emissions):
    n_states = len(initial_probs)
    emissions = iter(emissions)
    pt, emission = next(emissions)
    probs = emission*initial_probs
    stack = []
    state_idx = range(n_states)
    for t, emission in emissions:
        dt = t - pt
        pt = t
        trans_probs = scipy.linalg.expm(transition_rates*dt)*np.row_stack(probs)
        #print(trans_probs)
        most_likely_transitions = np.argmax(trans_probs, axis=0)
        probs = emission*trans_probs[most_likely_transitions, state_idx]
        probs /= np.sum(probs)
        stack.append(most_likely_transitions)
    state_seq = [np.argmax(probs)]
    while stack:
        most_likely_transitions = stack.pop()
        state_seq.append(most_likely_transitions[state_seq[-1]])
    state_seq.reverse()
    return state_seq


#emissions = np.zeros((len(classfeats), len(CLASSES)))

#initial = np.ones(len(transitions))
#initial /= np.sum(initial)
#initial = initial_states/np.sum(initial_states)
#initial = initial[:len(transitions)]

"""
total_pred = []
total_true = []
classfeats['pred_class'] = -1
for sid, sd in classfeats.groupby('session'):
    f = cf[classfeats.session == sid]
    #lv = np.log1p(sd.velocity)
    emissions = model.liks(f)
    emissions /= np.sum(emissions, axis=1).reshape(-1, 1)
    emissions = np.log(emissions)
    path = viterbi(np.log(initial), np.log(transitions), emissions)
    pred = [c+1 for c in path]
    classfeats.pred_class[classfeats.session == sid] = pred
    total_pred.extend(pred)
    total_true.extend(sd.cls)
    #print(np.sum(np.array(pred) == sd.cls)/len(pred))
    #print([int(c) for c in sd.cls])
    #print("Done")
"""

from segment_classifier import classify_gaze
def classify_session(sd):
    #est_classes, segmentation, seg_classes = classify_gaze(sd.ts, sd[['x', 'y']])
    #return est_classes, segmentation

    segmentation = pynslr.fit_gaze(sd.ts, sd[['x', 'y']])
    durations = np.array([float(np.diff(s.t)) for s in segmentation.segments])
    speeds = np.array([np.diff(s.x, axis=0).ravel() for s in segmentation.segments])/durations.reshape(-1, 1)
    directions = speeds/np.linalg.norm(speeds, axis=1).reshape(-1, 1)
    speed_diffs = [np.linalg.norm(s - sp) for (s, sp) in zip(speeds[:-1], speeds[1:])]
    speed_diffs = np.array([0.0] + list(speed_diffs))
    direction_cors = [np.dot(dp, d) for (d, dp) in zip(directions[:-1], directions[1:])]
    direction_cors = np.array([0.0] + list(direction_cors))
    direction_cors *= 1 - 1e-6
    direction_cors = np.arctanh(direction_cors)
    direction_cors[~np.isfinite(direction_cors)] = 0.0
    velocities = np.linalg.norm(speeds, axis=1)
    amplitudes = np.array([np.linalg.norm(np.diff(s.x, axis=0).ravel()) for s in segmentation.segments])
    prev_velocities = np.array([0] + list(velocities[:-1]))
    #amplitudes = [np.linalg.norm(np.diff(s.x, axis=0)) for s in segmentation.segments]
    #feats = np.array([np.log(velocities + 1e-6), np.log(speed_diffs + 1e-6)]).T
    #feats = np.array([np.log(velocities + 1e-6), direction_cors]).T
    feats = np.array([safelog(velocities), direction_cors]).T
    #feats = np.array([np.log(velocities + 1e-6), direction_cors]).T
    #feats = np.sqrt(feats)
    emissions = model.liks(feats)
    emissions /= np.sum(emissions, axis=1).reshape(-1, 1)
    path = viterbi(initial, transitions, emissions)

    #emissions = zip(segmentation.t, emissions)
    #path = viterbi_semi_ct(initial, transition_rates, emissions)
    
    #path = np.argmax(emissions, axis=1)
    scls = model.idxclass[path]
    est_classes = np.zeros(len(sd))
    for c, s in zip(scls, segmentation.segments):
        est_classes[s.i[0]:s.i[1]] = c
    return est_classes, segmentation, scls
    #return model.classidx[path], segmentation

for meta, session in sessions:
    #pred_cls, segmentation = classify_session(session)
    est_cls, segmentation, seg_cls = classify_session(session)
    acc = (np.sum(est_cls == session.cls)/len(session))
    session['est_cls'] = est_cls
    #continue
    session.x[session.cls > 4] = np.nan
    session.y[session.cls > 4] = np.nan
    
    plt.figure(0, figsize=(12, 6))
    #plt.suptitle("%s, %.2f"%(meta['stimtype'], acc))
    if meta['stimtype'] != 'videos': continue
    if meta['stimulus'] != 'video_triple_jump': continue
    
    tx_ax = plt.subplot2grid((2,2), (0, 1))
    tx_ax.set_ylabel("Horiz. gaze (degrees)")
    #tx_ax.set_xlabel("Time (seconds)")
    ty_ax = plt.subplot2grid((2,2), (1, 1), sharex=tx_ax)
    ty_ax.set_ylabel("Horiz. gaze (degrees)")
    ty_ax.set_xlabel("Time (seconds)")
    xy_ax = plt.subplot2grid((2,2), (0, 0), rowspan=2)
    xy_ax.set_xlabel("Horiz. gaze (degrees)")
    xy_ax.set_ylabel("Vert. gaze (degrees)")
    labels = []
    txplots = []
    def addtxplot(t, plots):
        for artist in plots:
            artist._origdatahack = artist.get_data()
            artist._timehack = np.array(t)
            txplots.append(artist)

    for c, cd in session.groupby('cls'):
        tx_ax.plot(cd.ts, cd.x, 'o', color=CLASS_COLORS[c])
        #ty_ax.plot(cd.ts, cd.y, 'o', color=CLASS_COLORS[c])
        addtxplot(cd.ts, xy_ax.plot(cd.x, cd.y, 'o', color=CLASS_COLORS[c]))
        #if c in INCLUDE_CLASSES:
        #    labels.append((txplots[-1], "%s"%(CLASSES[c])))
    
    for c, cd in session.groupby('est_cls'):
        pass
        #tx_ax.plot(cd.ts, cd.x, '.', color=CLASS_COLORS[c])
        #ty_ax.plot(cd.ts, cd.y, '.', color=CLASS_COLORS[c])
        #addtxplot(cd.ts, xy_ax.plot(cd.x, cd.y, '.', color=CLASS_COLORS[c]))
        #labels.append((txplots[-1], "%s (NSLR-HMM)"%(CLASSES[c])))
    
    #labels.append((plt.plot([], [], 'ko')[0], "Human"))
    #labels.append((plt.plot([], [], 'k.')[0], "NSLR-HMM"))

    #llabels = {}
    ts = session.ts.values
    recon = segmentation(ts)
    recon[session.cls > 4] = np.nan
    color = 'red'
    tx_ax.plot(ts, recon[:,0], '-', color=color, alpha=0.7)
    #ty_ax.plot(ts, recon[:,1], '-', color=color, alpha=0.7)
    addtxplot(ts, xy_ax.plot(recon[:,0], recon[:,1], '-', color=color, alpha=0.7))

    for s, cls in zip(segmentation.segments, seg_cls):
        color = CLASS_COLORS[cls]
        xs = np.array(s.x)
        if np.any(session.cls[s.i[0]:s.i[1]] > 4): continue
        #tx_ax.plot(s.t, xs[:,0], '-', color=color, alpha=0.7)
        #ty_ax.plot(s.t, xs[:,1], '-', color=color, alpha=0.7)
        ty_ax.plot(s.t, xs[:,0], '-', lw=3, color=color, alpha=0.7)
        #addtxplot(s.t, xy_ax.plot(xs[:,0], xs[:,1], '-', lw=3, color=color, alpha=0.7))
    
    import matplotlib

    for cls in INCLUDE_CLASSES:
        labels.append((
            matplotlib.patches.Rectangle([0, 0], 0, 0, facecolor=CLASS_COLORS[cls]),
            CLASSES[cls]
            ))

    labels.append((plt.scatter([], [], color='black', alpha=0.5), "Recording, human class"))
    labels.append((plt.plot([], [], '-', lw=3, color='black', alpha=0.5)[0], "NSLR segment, NSLR-HMM class"))
    
    labels.append((txplots[-1], "NSLR reconstruction"))
    #labels.extend(llabels.items())
    
    plt.gcf().legend(*zip(*labels), 'upper center', ncol=4)
    def hide_stuff(ax):
        lim = ax.get_xlim()
        for artist in txplots:
            odata = artist._origdatahack
            t = artist._timehack
            include = (t >= lim[0]) & (t <= lim[1])
            artist.set_data(odata[0][include], odata[1][include])
        plt.draw()
    tx_ax.callbacks.connect('xlim_changed', hide_stuff)
    ty_ax.callbacks.connect('xlim_changed', hide_stuff)


    plt.show()
    #for c, s in zip(pred_cls, segmentation.segments):
    #    plt.plot(

nslr_cls, keys = zip(*((s, m['recording']) for m, s in sessions if m['annotator'] == 'MN'))
nslr_cls = pd.concat(nslr_cls, keys=keys)
references['NSLR'] = nslr_cls.est_cls
references = references[(references.coderRA <= 4) & ((references.coderMN <= 4))]
#references[references == 4] = 1
#references[references == 3] = 1
#references[references == 0] = 1
references['stimtype'] = nslr_cls.stimtype
references = references.copy()
coders = 'coderMN', 'coderRA'

from sklearn.metrics import confusion_matrix, cohen_kappa_score

algos = list(references.columns)
algos.remove('stimtype')


# Collapse SEM to 1
"""
for algo in algos:
    if np.any(references[algo] == 2):
        references[algo][references[algo] != 2] = 1
    else:
        references[algo][references[algo] != 1] = 2
"""

algos = ['coderMN', 'NSLR', 'LNS', 'IVT', 'EK', 'IHMM', 'NH', 'IKF', 'IMST', 'IDTk', 'CDT', 'BIT']

"""
for stimtype, d in references.groupby('stimtype'):
    coder = coders[0]
    print(stimtype)
    print("\t".join([''] + algos))
    for cls in references[coder].unique():
        sys.stdout.write("%s\t"%cls)
        for algo in algos:
            kappa = cohen_kappa_score(d[coder] == cls, d[algo] == cls)
            sys.stdout.write("%.2f\t"%kappa)
        print()
"""
    
print("\t".join([''] + algos))
for cls in references[coders[0]].unique():
    sys.stdout.write("%s\t"%CLASSES[cls])
    for algo in algos:
        kappas = []
        for coder in coders:
            if coder == algo: continue
            cclss = (references[coder] == cls)
            aclss = (references[algo] == cls)
            kappa = cohen_kappa_score(cclss, aclss)
            kappas.append(kappa)
        kappa = np.mean(kappas)
        if not np.any(references[algo] == cls):
            sys.stdout.write("& -\t")
        else:
            sys.stdout.write("& %.2f\t"%kappa)
    print("\\\\")

print("TOTAL")
print("\t".join([''] + algos))
sys.stdout.write("\t")
for algo in algos:
    kappas = []
    for coder in coders:
        if coder == algo: continue
        cclss = references[coder] == cls
        aclss = references[algo] == cls
        kappas.append(cohen_kappa_score(cclss, aclss))
    sys.stdout.write("& %.2f\t"%np.mean(kappas))
print()

#print(references.columns)
#sys.exit()


data = pd.concat(s[1] for s in sessions)

#data.est_cls[data.est_cls == 4] = 1
#data.cls[data.cls == 4] = 1
#data.est_cls[data.est_cls == 3] = 1
#data.cls[data.cls == 3] = 1
annotators, ad = zip(*data.groupby('annotator'))
ad = [d.set_index(['recording', 'ts']) for d in ad]
basedata = ad[0].drop(['cls', 'annotator'], axis=1)
cls = pd.concat([d['cls'] for d in ad], axis=1, keys=annotators)
data = pd.concat([basedata, cls], axis=1)

#basedata = data[data.annotator = annotators[0]]
#ref_annot = data.annotator == 'RA'

data = data[data.MN.isin(INCLUDE_CLASSES) & data.RA.isin(INCLUDE_CLASSES)]

#data = data[data.cls <= 4]
#C = confusion_matrix(data.cls, data.est_cls)
#print(C.astype('float')/C.sum(axis=1)[:, np.newaxis])

for st, d in data.groupby('stimtype'):
    print('\t'.join((st[:5], 'HH', 'M1', 'M2')))
    for c in INCLUDE_CLASSES:
        hh = cohen_kappa_score(d.RA == c, d.MN==c)
        m1 = cohen_kappa_score(d.est_cls==c, d.RA == c)
        m2 = cohen_kappa_score(d.est_cls==c, d.MN == c)
        strs = ["%.2f"%v for v in (hh, m1, m2)]
        print('\t'.join([CLASSES[c][:5]] + strs))

#for st, d in data.groupby('stimtype'):
#    print("MACHINE", st)
#    for c in INCLUDE_CLASSES:
#        kappa = cohen_kappa_score(d.MN == c, d.est_cls == c)
#        print(CLASSES[c], kappa)

"""
lvs = np.log1p(classfeats.velocity)
fitprobs = []
edists = []
for i in range(len(initial)):
    feats = np.log1p(classfeats[classfeats.cls == (i + 1)])
    #plt.plot(feats.velocity, feats.prev_velocity, '.', label=CLASSES[i+1])
    #plt.hist(feats.speed_diff, bins=100, histtype='step', color=CLASS_COLORS[i+1])

    lv = np.log1p(feats.velocity)
    dist = scipy.stats.norm.fit(lv)
    dist = scipy.stats.norm(*dist)
    edists.append(dist)
    #emissions[:,i] = dist.pdf(lvs)
#plt.legend()
#plt.show()
"""

"""
rng = np.linspace(0, 3, 100)
for i in range(len(edists)):
    plt.plot(rng, edists[i].pdf(rng), color=CLASS_COLORS[i+1])
plt.show()
"""



#print("HMMACC", np.sum(np.array(total_pred) == np.array(total_true))/len(total_pred))
#C = confusion_matrix(total_true, total_pred)
#print(C.astype('float')/C.sum(axis=1)[:, np.newaxis])

"""
from sklearn.tree import DecisionTreeClassifier
import sklearn.tree
from io import BytesIO
tree = DecisionTreeClassifier().fit(classfeats[['velocity']], classfeats.cls)
viz = sklearn.tree.export_graphviz(tree, out_file=None, max_depth=2)
import pydot
png = pydot.graph_from_dot_data(viz).create_png()
plt.imshow(plt.imread(BytesIO(png)))
plt.show()
"""
#print(emissions)

#path = viterbi(initial, transitions, emissions)

#fts = classfeats.drop('cls', axis=1)
#fts = classfeats[['velocity', 'direction_diff',
#    #'prev_velocity'
#    #'prev_duration', 'prev_velocity'
#    ]]

#fts.velocity = np.log1p(fts.velocity)
#fts.prev_velocity = np.log1p(fts.prev_velocity)
#fts['duration'] = np.log(fts.duration.values + 0.01)
#fts['prev_duration'] = np.log(fts.prev_duration.values + 0.01)
#fts['amplitude'] = 1.0/(np.log1p(fts.amplitude.values) + 1)
#fts['prev_amplitude'] = 1.0/(np.log1p(fts.prev_amplitude.values) + 1)
#fts['prev_velocity'] = np.log1p(fts.prev_velocity.values)
#print(fts)
#twodee = PCA().fit_transform(fts)
#print(twodee)

from matplotlib.patches import Ellipse
def plot_ellipse(means, cov, **kwargs):
    vals, vecs = np.linalg.eig(cov)
    order = np.argsort(vals)[::-1]
    nstd = 2
    vals = vals[order]
    vecs = vecs[:,order]
    theta = np.degrees(np.arctan2(*vecs[:,0][::-1]))
    w, h = 2 * np.sqrt(vals*4.605)
    ell = Ellipse(xy=means, width=w, height=h,
            angle=theta, **kwargs)
    ell.set_facecolor('none')
    plt.gca().add_patch(ell)
    return ell



#print(model)

#predclass = model.classify(cf)
#print("ACCURACY", np.sum(predclass == (classfeats.cls - 1))/len(predclass))
"""
for cls in np.unique(classfeats.pred_class):
    idx = cls == classfeats.pred_class
    vs = cf.values[idx]
    plt.plot(vs[:,0], vs[:,1], 'o', color=CLASS_COLORS[model.classidx[cls-1]])
"""

from sklearn.covariance import MinCovDet

def plot_gauss_ellipse(data, **kwargs):
    robust = MinCovDet().fit(data)
    means = robust.location_
    cov = robust.covariance_
    cov = np.diag(np.diag(cov))
    #means = np.mean(data, axis=0)
    #cov = np.cov(data, rowvar=0)
    return plot_ellipse(means, cov, **kwargs)

for cls, feats in classfeats.groupby(('cls')):
    if len(feats) < 3:
        continue
    #td = twodee[classfeats.cls.values == cls]
    #print(td)
    if cls not in INCLUDE_CLASSES: continue
    #plt.figure(1)
    #plt.plot(td[:,0], td[:,1], '.', alpha=0.1, color=CLASS_COLORS[cls])
    #plt.figure(2)
    #plt.plot(np.log(feats.duration + 0.01), np.log1p(feats.velocity), '.', alpha=0.2,
    #        color=CLASS_COLORS[cls], label=CLASSES[cls])
    #plt.hist((1.0/feats.duration), bins=100, color=CLASS_COLORS[cls], histtype='step', normed=True)
    #plt.hist((feats.amplitude), bins=100, color=CLASS_COLORS[cls], histtype='step', normed=True)
    #plt.plot(np.log(feats.duration + 0.1), 1.0/(np.log1p(feats.amplitude) + 1), '.', color=CLASS_COLORS[cls], alpha=0.3)
    #plt.plot(np.log(feats.duration + 0.1), 1.0/(np.log1p(feats.velocity) + 1.0), '.', color=CLASS_COLORS[cls], alpha=0.3)
    
    #fs = np.sqrt(feats[['duration', 'amplitude']])
    #means = np.mean(fs.values, axis=0)
    #cov = np.cov(fs.values, rowvar=0)
    #dist = scipy.stats.multivariate_normal(means, cov)
    #dist = model.dist(cls)
    #plot_ellipse(dist.mean, dist.cov, color=CLASS_COLORS[cls], label=CLASSES[cls])
    
    color = CLASS_COLORS.get(cls, 'black')
    nf = cf[(classfeats.cls == cls)]
    plt.plot(nf[:,0], nf[:,1], '.', color=color, alpha=0.1)
    plot_gauss_ellipse(nf, color=color, label=CLASSES[cls])


    import matplotlib
plt.gca().xaxis.set_major_formatter(
        matplotlib.ticker.FuncFormatter(lambda x, pos: str(float("%.1g"%(10**x))))
        )
plt.gca().yaxis.set_major_formatter(
        #matplotlib.ticker.FuncFormatter(lambda x, pos: str(float("%.1g"%(10**x))))
        matplotlib.ticker.FuncFormatter(lambda x, pos: str(float("%.2g"%np.degrees(np.arccos(np.tanh(x)/(1 - 1e-6))))))
        )
plt.legend()
plt.xlabel(r'Segment velocity ($^\circ/s$)')
plt.ylabel(r'Inter-segment angle (degrees)')
plt.show()

"""
for _, session in sessions.iterrows():
    files = os.path.join(basedir, 'annotated_data', '*', "??%02i_*%s*_labelled*"%(session.subject, session.stimulus))
    files = glob(files)
    for f in files:
        #print(f)
        d = scipy.io.loadmat(f)

        w, h = d['ETdata']['screenDim'][0][0][0]
        dist = d['ETdata']['viewDist'][0][0][0]
        resx, resy = d['ETdata']['screenRes'][0][0][0]
        
        #print(f)
        if 'results' not in d:
            continue
        pixels_to_angles = angleizer(dist, w, h, resx, resy)
        dt = 1.0/int(d['ETdata']['sampFreq'])
        ed = pd.DataFrame(d['ETdata']['pos'][0][0], columns='ts,pupil_w,pupil_h,x,y,cls'.split(','))
        ed[['x', 'y']] = pixels_to_angles(ed[['x', 'y']].values)
        ed.ts = np.arange(0, len(ed))*dt
        print(session.rms)
        est = pynslr.fit_gaze(ed.ts, ed[['x', 'y']], optimize_noise=True)
        #est = pynslr.nslr2d(ed.ts, ed[['x', 'y']])

        ax = plt.subplot(2,1,1)
        plt.plot(est.t, np.array(est.x)[:,0], 'o-')
        for cls, cd in ed.groupby('cls'):
            plt.plot(cd.ts, cd.x, '.', label=CLASSES[cls])
        
        plt.subplot(2,1,2, sharex=ax)
        plt.plot(est.t, np.array(est.x)[:,1], 'o-')
        for cls, cd in ed.groupby('cls'):
            plt.plot(cd.ts, cd.y, '.', label=CLASSES[cls])
        plt.legend()
        plt.show()
        #for c
        #plt.scatter(ed[:,3])
        #plt.show()
"""
