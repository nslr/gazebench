import numpy as np
import simulate as sim
from simulate import random_targets, random_nystagmus, simulate_gaze
import matplotlib.pyplot as plt
import algorithms as algo
import fast_saccade_detectors as falgo
import saccade_detectors as salgo
import timeit
import itertools
#import pynslr
import cppimport
pynslr = cppimport.imp("pynslr")
import scipy.interpolate

def fixation_benchmark(algorithms, noise_levels=np.linspace(0.2, 5.0, 10)):
    for noise_level in noise_levels:
        for itr in range(3):
            time, signal, eye, target = simulate_gaze(sim.random_targets(), noise=noise_level)
            for i, algorithm in enumerate(algorithms):
                splits = algorithm(noise_level, thruth=eye)(time, signal)
                fit = algo.fit_fixations(time, signal, splits)
                error = np.mean(np.sqrt(np.sum((fit(time) - eye)**2, axis=1)))
                yield i, noise_level, error
    
def nystagmus_benchmark(algorithms, noise_levels=np.linspace(0.2, 5.0, 10)):
    for noise_level in noise_levels:
        for itr in range(3):
            time, signal, eye, target = simulate_gaze(sim.random_nystagmus(), noise=noise_level)
            for i, algorithm in enumerate(algorithms):
                splits = algorithm(noise_level, thruth=eye)(time, signal)
                fit = algo.fit_segments(time, signal, splits)
                error = np.mean(np.sqrt(np.sum((fit(time) - eye)**2, axis=1)))
                yield i, noise_level, error

"""
def reconstruct_naive(ts, segments):
    points = []
    for i in range(len(segments)):
        s = segments[i]
        points.append([s.t[0], s.x[0]])
        points.append([s.t[1], s.x[1]])
        if i < len(segments) - 1:
            points[-1][0] -= 0.001
    t, x = map(np.array, zip(*points))
    return scipy.interpolate.interp1d(t, x, axis=0)
"""

def reconstruct_naive(ts, xs, splits):
    points = []
    for s in splits:
        slc = slice(*s.i)
        t = ts[slc]
        x = xs[slc]
        coeffs = np.polyfit(t, x, 1)
        start = np.dot([t[0], 1], coeffs)
        points.append((t[0], start))
        end = np.dot([t[-1], 1], coeffs)
        points.append((t[-1], end))
    t, x = zip(*points)
    return scipy.interpolate.interp1d(t, x, axis=0)

import csegreg
def reconstruct_cont(ts, xs, splits):
    sis = [s.i[0] for s in splits]
    sis.append(splits[-1].i[1])
    fitx = csegreg.fit_segments_faster(ts, xs[:,0], sis)
    fity = csegreg.fit_segments_faster(ts, xs[:,1], sis)
    #fity = fitx
    fit = np.array([fitx, fity]).T
    #fit = csegreg.fit_segments_evenfaster(ts, xs, sis)
    sis[-1] -= 1
    return scipy.interpolate.interp1d(ts[sis], fit, axis=0)

def reconstruct(ts, segments):
    s = segments[0]
    points = [[s.t[0], np.copy(s.x[0]), s.i[1] - s.i[0]]]
    #points = []
    for i in range(len(segments)):
        s = segments[i]
        #points.append([s.t[0], s.x[0]])
        #points.append([s.t[1], s.x[1]])
        #if i < len(segments) - 1:
        #    points[-1][0] -= 0.001
        #continue
        #if len(points) == 0 or points[-1][0] != s.t[0]:
        #    points.extend(zip(s.t, s.x))
        #    return
        
        dur = s.t[1] - s.t[0]
        n = s.i[1] - s.i[0]
        #a = (s.x[1] - s.x[0])/dur
        #b = s.x[0]
        #a = a.reshape(b.shape)
        f = lambda t: (t - s.t[0])/dur*s.x[1] + (s.t[1] - t)/dur*s.x[0]
        #print(f(0.0))
        
        px = points[-1][1]
        pn = points[-1][2]
        # This should be calculated as a maximum likelihood estimate
        # with the regression points. 
        points[-1][1] = (px*(pn - 1) + s.x[0]*(n - 1))/(pn + n - 2)
        points[-1][2] = pn + n
        if n > 2:
            nt = ts[s.i[0] + 1]
            points.append([nt, f(nt), n])
            nt = ts[s.i[1] - 2]
            points.append([nt, f(nt), n])
        points.append([s.t[1], np.copy(s.x[1]), n])
    t, x, n = zip(*points)
    t = np.array(t)
    #for p in x: print(p)
    x = np.array(x)
    return scipy.interpolate.interp1d(t, x, axis=0)

def hacknols(noise_level, **kwargs):
    #noise_level = max(0.1, noise_level)
    def nols(ts, xs):
        #sts, sxs = pynslr.nslr_reconstruct_points(model, ts, xs)
        #model = pynslr.Nslr2d([noise_level, noise_level],
        #        pynslr.constant_penalty_split(0.0))

        #segments = pynslr.nslr2d(ts, xs, model)
        segments = pynslr.nslr2d(ts, xs, noise_level)
        #f = reconstruct_naive(ts, segments)
        #plt.plot(ts, f(ts)[:,0], 'k--')
        #return reconstruct(ts, segments.segments)
        x, y = zip(*segments.x)
        fitp = segments(ts)
        print(fitp.shape)
        x, y = zip(*fitp)
        #f = reconstruct_naive(ts, xs, segments.segments)
        f = reconstruct_cont(ts, xs, segments.segments)
        #return f
        plt.plot(f.x, f.y[:,0], 'bo-')
        #plt.plot(ts, x, '-')
        
        ts = []
        xs = []
        for s in segments.segments:
            ts.append(s.t[0])
            xs.append(s.x[0])
        ts.append(segments.segments[-1].t[1])
        xs.append(segments.segments[-1].x[1])

        #return segments
        return scipy.interpolate.interp1d(segments.t, segments.x, axis=0)
        return scipy.interpolate.interp1d(ts, xs, axis=0)
        splits = np.array(falgo.nols(ts, xs, [noise_level, noise_level]))
        #splits = np.unique(sorted(list(splits - 1) + list(splits)))
        if len(splits) == 0:
            return []
        return ts[splits]
    return nols

def hacknocs(noise_level, **kwargs):
    noise_level = max(0.1, noise_level)
    def iocs(ts, xs):
        splits = np.array(falgo.iocs(ts, xs, [noise_level, noise_level]))
        splits = np.unique(sorted(list(splits - 1) + list(splits)))
        if len(splits) == 0:
            return []
        return ts[splits]
    return iocs

def opt_velocity_threshold(noise_level, thruth):
    algorithm = algo.velocity_threshold
    def optimized(time, signal):
        def error(threshold):
            splits = algorithm(threshold)(time, signal)
            fit = algo.fit_fixations(time, signal, splits)
            error = np.mean(np.sqrt(np.sum((fit(time) - thruth)**2, axis=1)))
            return error
        thresholds = np.linspace(10, 500, 100)
        errors = list(map(error, thresholds))
        threshold = thresholds[np.argmin(errors)]
        return algo.velocity_threshold(threshold)(time, signal)
    
    return optimized

import pandas as pd

def test():
    algorithms = (
            ('NOLS', hacknols),
            ('NOCS', hacknocs),
            ('I-VT', opt_velocity_threshold),
            )
    anames, algos = list(zip(*algorithms))
    data = np.rec.fromrecords(list(nystagmus_benchmark(algos)), names='ai,noise_level,error')
    data = pd.DataFrame(data)
    for ai, d in data.groupby('ai'):
        md = d.groupby('noise_level').mean().reset_index()
        plt.plot(md.noise_level, md.error, '-', label=anames[ai])
    plt.xlabel('Measurement noise std (degrees)')
    plt.ylabel('Reconstruction error (degrees)')
    plt.legend()
    plt.show()
    return

def optimal_wiener(signal, thruth, size=1024):
    csd = lambda *a: scipy.signal.csd(*a, nperseg=size, window='hann')
    f, Pxx = csd(signal, signal)
    f, Psx = csd(thruth, signal)
    H = Psx/Pxx
    H = H * np.exp(-1j*2*np.pi/len(H)*np.arange(len(H))*(len(H)//2))  # shift for causal filter
    h = np.fft.irfft(H)
    return np.convolve(signal, h, mode='same')

from itertools import islice
def optimize_nols_penalty():
    noise_levels = np.linspace(0.2, 10.0, 5)[::-1]
    #durations = np.linspace(0.1, 1.0, 5)
    speeds = np.linspace(5.0, 60.0, 5)

    for noise_level, speed in itertools.product(noise_levels, speeds):
        noise_level = 1.0
        target = sim.random_nystagmus(max_speed=speed, max_distance=(10, 10))
        #target = sim.circular_target()
        res = list(simulate_gaze(target, noise=noise_level))
        for i in range(len(res)):
            res[i] = res[i]
        time, signal, eye, target = res
        #splits = hacknols(noise_level, thruth=eye)(time, signal)
        fit = hacknols(noise_level, thruth=eye)(time, signal)
        #fit = algo.fit_segments(time, signal, splits)
        error = np.mean(np.sqrt(np.sum((fit(time) - eye)**2, axis=1)))

        #savgol = scipy.signal.savgol_filter(signal, 101, 2, axis=0)
        wiener = optimal_wiener(signal[:,0], eye[:,0])
        #plt.plot(time, savgol[:,0])
        #plt.plot(time, wiener)
        #plt.plot(time, fit(time)[:,0], '-')
        #plt.plot(time, eye[:,0], '-')
        #plt.show()
        #plt.plot(time, fit(time)[:,0], 'r-')
        #plt.plot(fit.x, fit.y[:,0], 'r.-')
        plt.clf()
        plt.subplot(2,1,2)
        plt.plot(time, eye[:,0], 'r-', label='Gaze position')
        plt.plot(time, target[:,0], 'g--', label='Target')
        plt.plot(time, signal[:,0], 'k.', alpha=0.1, label='Measurement')
        plt.xlabel("Time (seconds)")
        plt.ylabel("Horiz. gaze position (degrees)")
        plt.legend()
        plt.show()


def test2():
    time, signal, eye, target = simulate_gaze(sim.random_nystagmus())
    #plt.plot(time, signal[:,0], '.')
    #plt.plot(time, eye[:,0])
    #plt.plot(time, target[:,0])
    #plt.show()
    #time, signal, eye, target = simulate_gaze(random_nystagmus())
    
    #splits = falgo.nols(time, signal)

    splits = np.array(falgo.iocs(time, signal))
    splits = np.unique(sorted(list(splits - 1) + list(splits)))
    splits = time[splits]
    fit = falgo.fit_segments(time, signal, splits)
    
    print(splits)
    #signal = target
    ax = plt.subplot(2,1,1)
    plt.plot(time, signal[:,0], '.')
    plt.plot(time, target[:,0], color='green')
    plt.plot(time, fit(time)[:,0])
    
    ax = plt.subplot(2,1,2, sharex=ax)
    plt.plot(time, signal[:,1], '.')
    plt.plot(time, target[:,1], color='green')
    plt.plot(time, fit(time)[:,1])
    #for s in splits:
    #    plt.axvline(s, color='black')
    
    """
    fit = fit(time)
    plt.subplot(1,2,2)
    plt.plot(signal[:,0], signal[:,1], '.')
    plt.plot(target[:,0], target[:,1], color='green')
    plt.plot(fit[:,0], fit[:,1], '.-')
    """
    plt.show()

if __name__ == '__main__':
    np.random.seed(0)
    optimize_nols_penalty()
    #test()
