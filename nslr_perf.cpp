//#include <armadillo>
#include "segmented_regression.hpp"
#include <vector>
#include <string>
#include <iostream>
#include <ctime>
#include <random>

int main(int argc, char **argv) {
	using Vector = Nslr2d::Vector;
	std::vector<std::string> args(argv+1, argv+argc);
	
	uint n = 1000000;
	if(args.size() > 0) n = std::stoi(args[0]);

	Vector noise_std = {0.0, 0.0};
	if(args.size() > 1) noise_std.array() += std::stod(args[1]);
	else noise_std.array() += 1.0;
	
	std::default_random_engine generator;
	std::normal_distribution<double> noise_dist(0.0, noise_std(0, 0));
	Vector noise_vec;
	Vector m_vec;
	Vector position = {0.0, 0.0};
	auto renoise = [&] {
		noise_vec(0, 0) = noise_dist(generator);
		noise_vec(1, 0) = noise_dist(generator);
		m_vec = position + noise_vec;
	};
	
	double penalty = 6.0;
	if(args.size() > 2) penalty = std::stod(args[2]);
	
	double dt = 1.0/100.0;
	if(args.size() > 3) dt = std::stod(args[3]);

	//Nslr2d fitter(noise_std, split_rate);
	Nslr2d fitter(noise_std, penalized_exponential_split(penalty));
	//Nslr2d fitter(noise_std, );
	
	if(args.size() > 4) position.array() += std::stod(args[4]);
	

	auto duration = n/dt;
	Array<double, -1, 1> ts =  Array<double, -1, 1>::LinSpaced(n, 0.0, duration);
	Array<double, -1, 2> xs = Array<double, -1, 2>::Zero(n, 2);
	xs.col(0) = (ts/10.0).sin();
	//xs.col(1) = (ts/10.0).cos();
	auto start_time = clock();
	auto segments = nslr2d(ts, xs, fitter);
	
	/*
	auto start_time = clock();
	renoise();
	fitter.measurement(0, m_vec);
	//std::cerr << "noise_std: " << noise_std << ", split rate: " << split_rate << std::endl;
	Vector msum;
	for(uint i=1; i < n; ++i) {
		position.array() = std::sin(i*dt/10.0);
		renoise();
		fitter.measurement(dt, m_vec);
		msum += m_vec;
	}*/
	std::cerr << n/(double(clock()-start_time)/CLOCKS_PER_SEC) << " samples per second" << std::endl;
	std::cerr << std::distance(fitter.hypotheses.begin(), fitter.hypotheses.end()) << " hypotheses alive" << std::endl;
}
